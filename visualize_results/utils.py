import re
import numpy as np
import numpy.typing as npt
from imageio import v3 as iio


def natsort(s, _nsre=re.compile("([0-9]+)")):
    return [
        int(text) if text.isdigit() else text.lower() for text in _nsre.split(str(s))
    ]


# reference: https://stackoverflow.com/questions/14465297/connected-component-labeling-implementation
# perform depth first search for each candidate/unlabeled region
dx = [+1, 0, -1, 0]
dy = [0, +1, 0, -1]


def dfs(mask: npt.NDArray, x: int, y: int, labels: npt.NDArray, current_label: int):
    n_rows, n_cols = mask.shape
    if x < 0 or x == n_rows:
        return
    if y < 0 or y == n_cols:
        return
    if labels[x][y] or not mask[x][y]:
        return  # already labeled or not marked with 1 in image

    # mark the current cell
    labels[x][y] = current_label

    # recursively mark the neighbors
    for direction in range(4):
        dfs(mask, x + dx[direction], y + dy[direction], labels, current_label)


def find_components(mask: npt.NDArray, start_label=1, min_mask_pixels=16):
    label = start_label

    n_rows, n_cols = mask.shape
    labels = np.zeros(mask.shape, dtype=np.int8)

    for i in range(n_rows):
        for j in range(n_cols):
            if not labels[i][j] and mask[i][j]:
                dfs(mask, i, j, labels, label)
                label += 1

    max_label = labels.max()
    subtraction_matrix = np.zeros_like(labels)
    for l in range(start_label, max_label + 1):
        if (labels == l).sum() < min_mask_pixels:
            labels[labels == l] = 0
            subtraction_matrix[labels > l] += 1

    labels -= subtraction_matrix
    labels[labels < start_label] = 0

    return labels.astype(np.uint8)


# https://stackoverflow.com/questions/31400769/bounding-box-of-numpy-array
def bbox(img):
    img = img.numpy()
    max_x, max_y = img.shape
    rows = np.any(img, axis=1)
    cols = np.any(img, axis=0)
    rmin, rmax = np.where(rows)[0][[0, -1]]
    cmin, cmax = np.where(cols)[0][[0, -1]]

    rmin = rmin - 1 if rmin > 0 else rmin
    cmin = cmin - 1 if cmin > 0 else cmin
    rmax = rmax + 1 if rmax < max_x else rmax
    cmax = cmax + 1 if cmax < max_y else cmax

    return rmin, rmax, cmin, cmax


def extract_single_masks(labels: npt.NDArray):
    masks = []
    for l in range(1, labels.max() + 1):
        # TODO: ignore label 0 ????
        mask = (labels == l).astype(np.int8)
        max_x, max_y = mask.shape
        pixel_sum = mask.sum()
        if pixel_sum > 128 and pixel_sum < 512:
            # print(pixel_sum)
            rmin, rmax, cmin, cmax = bbox(mask)
            rmin = rmin - 1 if rmin > 0 else rmin
            cmin = cmin - 1 if cmin > 0 else cmin
            rmax = rmax + 1 if rmax < max_x else rmax
            cmax = cmax + 1 if cmax < max_y else cmax

            aspect_ratio = (rmax - rmin) / (cmax - cmin)
            if aspect_ratio > 0.5 and aspect_ratio < 2.0:
                masks.append(mask[rmin : rmax + 1, cmin : cmax + 1])
            else:
                pass
                # print("rejected due to aspect ratio")

    return masks
