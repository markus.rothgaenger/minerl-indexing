import glob
import os
import sys
from typing import Generator
import matplotlib
from matplotlib.pyplot import fill


import numpy as np
from PIL import Image as img
from PIL.Image import Image
import torch
from torchvision.transforms import transforms
from complexity import (
    compression_measure,
    fft_measure,
    multidim_complexity,
    pixelwise_complexity_measure,
)
from matplotlib import cm

from data import get_dino_transforms
from utils import find_components, natsort
from models import load_models

sys.setrecursionlimit(1000000)
n_clusters = 5


def create_vis(
    n_imgs, layer_range=range(12), sort=False
) -> Generator[Image, Image, None]:
    """
    yields three channel PIL image

    original by @wlad
    """
    inp = "./data/dino/"
    out = "./results/comp_fft_px_sorted/"
    os.makedirs(out, exist_ok=True)
    rltrenner = img.open("./data/static/rltrenner.png")
    rltrenner = rltrenner.convert("RGB")

    yseperator = 20
    print("Creating images")
    for i in range(n_imgs):  # TODO: maybe add tqdm again..
        imagesvert = []
        name = "img" + str(i)

        aimg = img.open(inp + name + ".png")
        aimg = aimg.convert("RGB")

        for depth in layer_range:
            imagesinline = [aimg]

            for attention_type in ["q", "k", "v"]:
                complexities = []
                _attention_images = []

                img_paths = sorted(
                    glob.glob(
                        os.path.join(
                            inp, f"{name}{attention_type}depth{str(depth)}head*.png"
                        )
                    ),
                    key=natsort,
                )

                for path in img_paths:
                    image = img.open(path)

                    if sort:
                        image, complexity = yield image
                    else:
                        image = yield image

                    yield

                    # image = image.convert("RGB")

                    image = image.resize((480, 480), resample=img.NEAREST)

                    _attention_images.append(image)
                    if sort:
                        complexities.append(complexity)

                if sort:
                    sort_idx = np.argsort(complexities)
                    _attention_images = [_attention_images[i] for i in sort_idx]

                _attention_images.insert(0, rltrenner)
                imagesinline.extend(_attention_images)

            widths, heights = zip(*(i.size for i in imagesinline))

            total_width = sum(widths)
            max_height = max(heights)

            vertical_img = img.new("RGB", (total_width, max_height))

            x_offset = 0
            for im in imagesinline:
                vertical_img.paste(im, (x_offset, 0))
                x_offset += im.size[0]
            imagesvert.append(vertical_img.convert("RGB"))

        widths, heights = zip(*(i.size for i in imagesvert))

        total_height = sum(heights) + (len(layer_range) * yseperator)
        max_width = max(widths)

        final_img = img.new("RGB", (max_width, total_height))

        y_offset = 0
        for im in imagesvert:
            final_img.paste(im, (0, y_offset))
            y_offset += im.size[1] + yseperator

        final_img.save(os.path.join(out, "img" + str(i) + ".png"))


def multi_dim(max_norm=True, sort=False):
    img_transformer = get_dino_transforms()
    image_generator = create_vis(n_imgs=25, sort=sort)
    color_map = cm.get_cmap("plasma")
    model_bn8, model_bn32 = load_models()

    for img in image_generator:
        np_img = np.array(img)
        rgb_np_img = np.dstack((np_img, np_img, np_img))
        cluster_img = (np_img / (255 / (n_clusters - 1))).astype(np.int8)

        start_label = 1
        complexiy_vectors = []
        all_labels = np.zeros_like(np_img, dtype=np.uint8)

        for i in range(n_clusters):
            cluster_image = (cluster_img == i).astype(np.int8)

            labels = find_components(cluster_image, start_label)

            for l in range(start_label, labels.max() + 1):
                mask = (labels == l).astype(np.float32)
                if not mask.sum() > 0:
                    continue

                normalized_img = img_transformer(transforms.F.to_pil_image(mask))

                complexity_vector = multidim_complexity(
                    normalized_img,
                    [
                        ("compression", compression_measure, [True]),
                        ("fft", fft_measure, []),
                        (
                            "pixelwise",
                            pixelwise_complexity_measure,
                            [model_bn32, model_bn8, True],
                        ),
                    ],
                )

                complexiy_vectors.append(complexity_vector)

            start_label = labels.max() + 1 if labels.max() > 0 else start_label
            all_labels += labels

        complexity_vectors = torch.stack(complexiy_vectors, dim=0)

        if max_norm:
            maxs = complexity_vectors.max(dim=0)
            complexity_vectors /= maxs.values

        complexity_norm = torch.linalg.vector_norm(complexity_vectors, dim=1)

        # rank sorted complexities descending
        color_idx = torch.argsort(torch.argsort(-complexity_norm))

        color_values = (color_idx / len(color_idx)).numpy()

        rgb_np_img[all_labels == 0, :] = np.array([0, 0, 0])

        for l in range(1, all_labels.max() + 1):
            r, g, b = matplotlib.colors.to_rgb(color_map(color_values[l - 1]))
            rgb_np_img[all_labels == l, :] = np.array([r, g, b]) * 255.0

        if sort:
            image_generator.send(
                (transforms.F.to_pil_image(rgb_np_img), complexity_norm.sum())
            )
        else:
            image_generator.send(transforms.F.to_pil_image(rgb_np_img))


def single_dim(sort=False):
    img_transformer = get_dino_transforms()
    image_generator = create_vis(n_imgs=25, sort=sort)
    color_map = cm.get_cmap("plasma")
    model_bn8, model_bn32 = load_models()

    for img in image_generator:
        np_img = np.array(img)
        rgb_np_img = np.dstack((np_img, np_img, np_img))
        cluster_img = (np_img / (255 / (n_clusters - 1))).astype(np.int8)

        start_label = 1
        complexities = []
        all_labels = np.zeros_like(np_img, dtype=np.uint8)

        for i in range(n_clusters):
            cluster_image = (cluster_img == i).astype(np.int8)

            labels = find_components(cluster_image, start_label)

            for l in range(start_label, labels.max() + 1):
                mask = (labels == l).astype(np.float32)
                if not mask.sum() > 0:
                    continue

                normalized_img = img_transformer(transforms.F.to_pil_image(mask))

                # complexity, _ = compression_measure(normalized_img, True)
                complexity, _ = pixelwise_complexity_measure(
                    normalized_img, model_bn32, model_bn8, fill_ratio_norm=True
                )
                # complexity, _ = fft_measure(normalized_img)
                complexities.append(complexity)

            start_label = labels.max() + 1 if labels.max() > 0 else start_label
            all_labels += labels

        # rank sorted complexities descending
        color_idx = np.argsort(np.argsort(-np.array(complexities)))

        color_values = color_idx / len(color_idx)

        rgb_np_img[all_labels == 0, :] = np.array([0, 0, 0])

        for l in range(1, all_labels.max() + 1):
            r, g, b = matplotlib.colors.to_rgb(color_map(color_values[l - 1]))
            rgb_np_img[all_labels == l, :] = np.array([r, g, b]) * 255.0

        if sort:
            image_generator.send(
                (transforms.F.to_pil_image(rgb_np_img), np.sum(complexities))
            )
        else:
            image_generator.send(transforms.F.to_pil_image(rgb_np_img))


if __name__ == "__main__":
    multi_dim(sort=True)
