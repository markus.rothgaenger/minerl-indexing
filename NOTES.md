# 1. how do they solve the problem of the same visual representation in different points of a scene?
- they do not solve that problem
- but: they use current observation and goal pairs to determine the feasibility of a traversal

# 2. what kind of encoder/decoder they used? what is the exact architecture?
- mobilenet
- reference: Appendix B.2

## encoding

#### 4 layers
- concatenate current and goal observation along channel dim -> (6, 160, 120)
- **MobileNet** encoder -> (1024)
- fully connected layer, exponential activation of the variance -> mean (64) and variance (64) 
- 64dim stochastic, context-conditioned representation of the goal *z_t^g* -> mean and variance of normal distribution


## decoding
#### 4 layers
- context (current observation processed by second **MobileNet** and *z_t^g*)
	- reparametrization trick used to sample from latent space (otherwise we cannot backpropagate through random node!)
	- use concatenated encodings (processed current observation and sample from *z_t^g* => 1024 + 64 = 1088dim) to learn optimal action and distance to goal.
	- fully connected layer yields mean (3dim) and variance (3dim) of decoder
	- => mean is split into action (2dim) and distance (1dim)!

# 3. what do they do with the latent point (encoder output)?
- calculate prior from mean and variance -> feasibility of goal form current observation
	- set as subgoal if feasible enaugh
	- use latent point to calculate best action and distance -> navigate to it
- if not feasible, decide wether robot is at frontier of map
	- if at frontier, imagine next goal using the prior (sample from distribution)
	- if not at frontier, go to frontier (neighbouring node in graph that was least visited)

# 4. how do they differentiate whether the latent point is a unique representation of a landmark useful for navigation or just a not meaningful view (e.g. grass + sky)?
- they do not. but they always use current and goal observation to encode to latent space. 
- therefor, a latent point does not represent a landmark but the feasibility of a traversal from current to goal observation

# 5. how do they close loops in the graph
- a node for a new observation is added and for each other node (=goal) an edge is added with a weight according to the estimated distance
from the current to the goal observation

# 7. any presentation/unofficial implementation?
- none :(

# other

- encoder prior is multivariate (standard) normal distribution with mean 0 and variance identity!
- Alg 1. line 9 -> random sample from prior.

# MobileNet
- lightweight deep networks
- depthwise + pointwise convolution filters combined
- 8 to 9 times less computation at low reduction in accuracy
- even less computation by reducing channels and resolution width and height
	- factor alpha reduces the number of input channels
	- factor p reduces the input resolution


# questions
- is is said, that *z_g^t* is representing the ``stochastic, context-conditioned representation of the goal that uses 64-dimensions each to represent the mean and diagonal covariance of a Gaussian distribution``
- but would that not result in an 2x64dim vector? in other places, *z_g^t* is represented as a 64 dim vector..
- what is BatchNorm?



# sorting stuff
- fft -> simple shapes are complex :/


# measure metric
- https://ieeexplore.ieee.org/document/4014089 (kolmogorov reference, this is the old paper I implemented..)
- https://www.researchgate.net/publication/325207188_Exploring_2D_Shape_Complexity
	- criteria for complexity of shapes (circle should have lowest complexity)
	- clustering based on similair complexity
	- no "correct" ordering

- https://www.researchgate.net/publication/3308308_MPEG-7_Visual_shape_description
	- very old but promising! database included, not sure how this is sorted/classified....		

- http://summergeometry.org/sgi2021/2d-shape-complexity/
	- similarity between user and metric rankings..



## thoughts
- maybe use user rankings


