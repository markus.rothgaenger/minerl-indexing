import logging
import os
import json
from multiprocessing import Pool, set_start_method
from multiprocessing.sharedctypes import Value
from random import random
from time import perf_counter_ns

import faiss
import imageio
import matplotlib.pyplot as plt
import minerl
import numpy as np
import torch
import torch.nn as nn
import torchvision.transforms as T
from matplotlib.backends.backend_agg import FigureCanvasAgg
from minerl.data import BufferedBatchIter
from torch import Tensor
from torchvision.models import resnet18

set_start_method('fork')
os.environ['MINERL_DATA_ROOT'] = '/home/markus/minerl'
logging.basicConfig(level=logging.ERROR)
rng = np.random.default_rng()

FPS = 30
SNIPPET_LENGTH_S = 10
MAX_FRAMES = FPS * SNIPPET_LENGTH_S
NUM_MATCHES = 9
TIMESTEP_THRESHOLD = 200

OUT_DIR = f'{os.getcwd()}/indexing/output'
global observation_trajectory_mapping, model, data_pipeline, iterator, test_map, index


class MobileNetV2(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = torch.hub.load(
            'pytorch/vision:v0.11.1', 'mobilenet_v2', pretrained=True).eval()
        self.transforms = nn.Sequential(
            T.Resize([224, 224]),
            T.ConvertImageDtype(torch.float),
            T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        )
        if torch.cuda.is_available():
            print('cuda enabled!')
            self.model.to('cuda')

    def eval(self, x: Tensor) -> Tensor:
        with torch.no_grad():
            x = self.transforms(x)
            x = x.unsqueeze(0)
            if torch.cuda.is_available():
                x = x.to('cuda')
            return self.model(x)


class ResNet(nn.Module):
    def __init__(self):
        super().__init__()
        self.resnet18 = resnet18(pretrained=True, progress=False).eval()
        self.transforms = nn.Sequential(
            T.Resize([224, 224]),
            T.ConvertImageDtype(torch.float),
            T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        )

    def eval(self, x: Tensor) -> Tensor:
        with torch.no_grad():
            x = self.transforms(x)
            x = x.unsqueeze(0)
            return self.resnet18(x)

    def forward(self, x: Tensor) -> Tensor:
        with torch.no_grad():
            x = self.transforms(x)
            x = x.unsqueeze(0)
            y_pred = self.resnet18(x)
            return y_pred.argmax(dim=1)


def train():
    global observation_trajectory_mapping, model, data_pipeline, iterator, test_map, index
    episode_i = 0

    start = perf_counter_ns()
    for trajectory in iterator.available_trajectories[:10]:
        i = 0

        for current_state, _, _, _, done in data_pipeline.load_data(trajectory):
            activation = model.eval(Tensor(current_state['pov'].T))

            if (done):
                episode_i += 1

            if random() > 0.2:
                index.add(activation.cpu().numpy())
                if observation_trajectory_mapping is None:
                    observation_trajectory_mapping = np.array(
                        [[trajectory, i]], dtype=object)
                else:
                    observation_trajectory_mapping = np.append(
                        observation_trajectory_mapping, [[trajectory, i]], axis=0)
            else:
                if test_map.get(trajectory) is None:
                    test_map[trajectory] = np.array(
                        [[activation.cpu().numpy(), i]], dtype=object)
                else:
                    test_map[trajectory] = np.append(
                        test_map[trajectory], [[activation.cpu().numpy(), i]], axis=0)

            i += 1

    end = perf_counter_ns()
    print(f"training took {(end - start)/1e6} ms!\n")


def get_subplot_index(match_index):
    if match_index > 5:
        return match_index + 10
    if match_index > 2:
        return match_index + 7
    return match_index + 4


def create_video(args):
    global observation_trajectory_mapping, model, data_pipeline, iterator, test_map, index
    trj, test = args
    with test_index.get_lock():
        test_index.value += 1
        base_path = f"{OUT_DIR}/{test_index.value:02}"
    _ot, _it = test

    match_count = NUM_MATCHES * 3
    _, I = index.search(_ot, match_count)

    # if not os.path.exists(base_path):
    #     os.mkdir(base_path)

    match_frames = np.empty(NUM_MATCHES, dtype=object)
    matched_trajectories = {}
    record = 0

    for match in range(len(I[0])):
        _trj, _timestep = observation_trajectory_mapping[I[0][match]]

        already_matched_trj = matched_trajectories.get(_trj)
        skip_match = False

        if already_matched_trj is not None:
            for timestamp in already_matched_trj:
                if np.abs(int(_timestep) - int(timestamp)) < TIMESTEP_THRESHOLD:
                    skip_match = True
                    break

            if skip_match:
                continue

            already_matched_trj.append(_timestep)
        else:
            matched_trajectories[_trj] = [_timestep]

        _data_ts = 0
        recorded_frames = 0
        frames = np.empty(MAX_FRAMES, dtype=object)
        for current_state, action, reward, next_state, done in data_pipeline.load_data(_trj):
            _data_ts += 1

            # skip to matched frame index
            if _data_ts < int(_timestep):
                continue

            frames[recorded_frames] = current_state['pov']
            recorded_frames += 1
            if recorded_frames >= MAX_FRAMES-1:
                break

        match_frames[record] = frames
        record += 1

        if record > NUM_MATCHES - 1:
            break

    if record < NUM_MATCHES:
        print('less than 9 matched!')

    _data_ts = 0
    recorded_frames = 0
    main_frames = np.empty(MAX_FRAMES, dtype=object)
    for current_state, action, reward, next_state, done in data_pipeline.load_data(trj):
        _data_ts += 1

        # skip to matched frame index
        if _data_ts < int(_it):
            continue

        main_frames[recorded_frames] = current_state['pov']
        recorded_frames += 1
        if recorded_frames >= MAX_FRAMES-1:
            break

    last_main_frame = main_frames[0]
    last_frame = np.array([f[0] if f is not None else np.zeros(
        main_frames[0].shape) for f in match_frames])

    with imageio.get_writer(f"{base_path}-{trj}.webm", mode="I", fps=30, codec='libvpx-vp9', output_params=['-crf 45']) as writer:
        for frame in range(MAX_FRAMES):
            fig = plt.figure()
            canvas = FigureCanvasAgg(fig)
            ax_main = plt.subplot(1, 2, 1)

            new_main_frame = main_frames[frame]

            if new_main_frame is None:
                ax_main.imshow(last_main_frame)
            else:
                ax_main.imshow(new_main_frame)
                last_main_frame = new_main_frame

            ax_main.axis('off')
            ax_main.margins(0)

            for match in range(NUM_MATCHES):
                ax = plt.subplot(3, 6, get_subplot_index(match))
                frames = match_frames[match]

                new_frame = frames[frame] if frames is not None else None

                if new_frame is None:
                    ax.imshow(last_frame[match])
                else:
                    ax.imshow(new_frame)
                    last_frame[match] = new_frame

                ax.axis('off')
                ax.margins(0)

            fig.tight_layout(pad=0)

            # https://stackoverflow.com/questions/35355930/matplotlib-figure-to-image-as-a-numpy-array
            canvas.draw()
            s, (width, height) = canvas.print_to_buffer()

            img = np.frombuffer(s, np.uint8).reshape((height, width, 4))
            writer.append_data(img)

            fig.clear()
            plt.close()

    return True


def _init(_index):
    global test_index
    test_index = _index

# https://stackoverflow.com/questions/26646362/numpy-array-is-not-json-serializable


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


if __name__ == '__main__':
    global observation_trajectory_mapping, model, data_pipeline, iterator, test_map, index

    data = minerl.data.make('MineRLTreechop-v0')
    model = MobileNetV2()
    iterator = BufferedBatchIter(data)
    data_pipeline = iterator.data_pipeline

    if not os.path.exists('./tmp'):
        os.mkdir('./tmp')

    # TODO: sys.args..
    if False:
        index = faiss.IndexFlatL2(1000)
        observation_trajectory_mapping = None
        test_map = dict()

        train()

        faiss.write_index(index, './tmp/index.idx')

        with open('./tmp/observation_trajectory_mapping.json', 'w') as fp:
            json.dump(observation_trajectory_mapping, fp, cls=NumpyEncoder)

        with open('./tmp/test_map.json', 'w') as fp:
            json.dump(test_map, fp, cls=NumpyEncoder)

    else:
        index = faiss.read_index('tmp/index.idx')

        with open('tmp/observation_trajectory_mapping.json', 'r') as fp:
            observation_trajectory_mapping = json.load(fp)

        with open('tmp/test_map.json', 'r') as fp:
            test_map = json.load(fp)

        for key in test_map.keys():
            vals = [[np.asarray(_trj, dtype=np.float32), _it]
                    for _trj, _it in test_map[key]]
            test_map[key] = np.asarray(vals, dtype=object)

    test_index = Value('i', 0)

    with Pool(10, initializer=_init, initargs=(test_index,)) as pool:
        for trj in rng.choice(list(test_map), 10):
            test_o = [(trj, test) for test in rng.choice(test_map[trj], 10)]
            res = pool.map(create_video, test_o)
            print(res)
