import os

# from zlib import compress
from bz2 import compress
from typing import Callable

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.transforms import Transform
import numpy as np
import numpy.typing as npt
import torch
import torch.nn.functional as F
from kornia.morphology import closing
from PIL import Image
from torch import Tensor, nn
from torch.optim import Adam
from torch.utils.data import DataLoader, RandomSampler, Dataset
from torchvision.datasets import ImageFolder
from torchvision.transforms import transforms
from torchvision.utils import make_grid, save_image

device = torch.device("cuda")
matplotlib.use("Agg")

dx = [+1, 0, -1, 0]
dy = [0, +1, 0, -1]


# perform depth first search for each candidate/unlabeled region
# reference: https://stackoverflow.com/questions/14465297/connected-component-labeling-implementation
def dfs(mask: npt.NDArray, x: int, y: int, labels: npt.NDArray, current_label: int):
    n_rows, n_cols = mask.shape
    if x < 0 or x == n_rows:
        return
    if y < 0 or y == n_cols:
        return
    if labels[x][y] or not mask[x][y]:
        return  # already labeled or not marked with 1 in image

    # mark the current cell
    labels[x][y] = current_label

    # recursively mark the neighbors
    for direction in range(4):
        dfs(mask, x + dx[direction], y + dy[direction], labels, current_label)


def find_components(mask: npt.NDArray):
    label = 0

    n_rows, n_cols = mask.shape
    labels = np.zeros(mask.shape, dtype=np.int8)

    for i in range(n_rows):
        for j in range(n_cols):
            if not labels[i][j] and mask[i][j]:
                label += 1
                dfs(mask, i, j, labels, label)

    return labels


# https://stackoverflow.com/questions/31400769/bounding-box-of-numpy-array
def bbox(img: Tensor):
    img = img.numpy()
    max_x, max_y = img.shape
    rows = np.any(img, axis=1)
    cols = np.any(img, axis=0)
    rmin, rmax = np.where(rows)[0][[0, -1]]
    cmin, cmax = np.where(cols)[0][[0, -1]]

    rmin = rmin - 1 if rmin > 0 else rmin
    cmin = cmin - 1 if cmin > 0 else cmin
    rmax = rmax + 1 if rmax < max_x else rmax
    cmax = cmax + 1 if cmax < max_y else cmax

    return rmin, rmax, cmin, cmax


def extract_single_masks(labels: npt.NDArray):
    masks = []
    for l in range(labels.max() + 1):
        mask = (labels == l).astype(np.int8)
        rmin, rmax, cmin, cmax = bbox(mask)
        masks.append(mask[rmin : rmax + 1, cmin : cmax + 1])

    return masks


class VAE(nn.Module):
    """
    https://github.com/pytorch/examples/blob/main/vae/main.py
    """

    def __init__(self, bottleneck=2, image_dim=4096):
        super(VAE, self).__init__()

        self.bottleneck = bottleneck
        self.image_dim = image_dim

        self.prelim_encode = nn.Sequential(
            nn.Flatten(), nn.Linear(image_dim, 400), nn.ReLU()
        )
        self.encode_mu = nn.Sequential(nn.Linear(400, bottleneck))
        self.encode_logvar = nn.Sequential(nn.Linear(400, bottleneck))

        self.decode = nn.Sequential(
            nn.Linear(bottleneck, 400),
            nn.ReLU(),
            nn.Linear(400, image_dim),
            nn.Sigmoid(),
        )

    def encode(self, x):
        # h1 = F.relu(self.encode(x))
        # return self.encode_mu(h1), self.encode_logvar(h1)
        x = self.prelim_encode(x)
        return self.encode_mu(x), self.encode_logvar(x)

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return mu + eps * std

    def forward(self, x):
        mu, logvar = self.encode(x)
        z = self.reparameterize(mu, logvar)
        return self.decode(z), mu, logvar

    # Reconstruction + KL divergence losses summed over all elements and batch
    def loss(self, recon_x, x, mu, logvar):
        BCE = F.binary_cross_entropy(recon_x, x.view(-1, 4096), reduction="sum")

        # see Appendix B from VAE paper:
        # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
        # https://arxiv.org/abs/1312.6114
        # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
        KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

        return BCE + KLD


class CONVVAE(nn.Module):
    def __init__(
        self,
        bottleneck=2,
    ):
        super(CONVVAE, self).__init__()

        self.bottleneck = bottleneck
        self.feature_dim = 6 * 6 * 64

        self.conv1 = nn.Sequential(
            nn.Conv2d(1, 16, 5),
            nn.ReLU(),
            nn.MaxPool2d((2, 2)),  # -> 30x30x16
        )
        self.conv2 = nn.Sequential(
            nn.Conv2d(16, 32, 3),
            nn.ReLU(),
            nn.MaxPool2d((2, 2)),  # -> 14x14x32
        )
        self.conv3 = nn.Sequential(
            nn.Conv2d(32, 64, 3),
            nn.ReLU(),
            nn.MaxPool2d((2, 2)),  # -> 6x6x64
        )
        # self.conv4 = nn.Sequential(
        #     nn.Conv2d(32, self.bottleneck, 5),
        #     nn.ReLU(),
        #     nn.MaxPool2d((2, 2), return_indices=True),  # -> 1x1xbottleneck
        # )

        self.encode_mu = nn.Sequential(
            nn.Flatten(),
            nn.Linear(self.feature_dim, self.bottleneck),
        )
        self.encode_logvar = nn.Sequential(
            nn.Flatten(), nn.Linear(self.feature_dim, self.bottleneck)
        )

        self.decode_linear = nn.Linear(self.bottleneck, self.feature_dim)

        # self.decode4 = nn.Sequential(
        #     nn.ConvTranspose2d(self.bottleneck, 32, 5),
        #     nn.ReLU(),
        # )
        self.decode3 = nn.Sequential(
            nn.ConvTranspose2d(64, 64, 2, stride=2),  # -> 12x12x64
            nn.ReLU(),
            nn.ConvTranspose2d(64, 32, 3),  # -> 14x14x32
            nn.ReLU(),
        )
        self.decode2 = nn.Sequential(
            nn.ConvTranspose2d(32, 32, 2, stride=2),  # -> 28x28x32
            nn.ReLU(),
            nn.ConvTranspose2d(32, 16, 3),  # -> 30x30x16
            nn.ReLU(),
        )
        self.decode1 = nn.Sequential(
            nn.ConvTranspose2d(16, 16, 2, stride=2),  # -> 60x60x16
            nn.ReLU(),
            nn.ConvTranspose2d(16, 1, 5),  # -> 64x64x1
            nn.Sigmoid(),
        )

    def encode(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        # x, idx4 = self.conv4(x)
        mu = self.encode_mu(x)
        logvar = self.encode_logvar(x)

        return mu, logvar

    def decode(self, z: Tensor):
        z = self.decode_linear(z)
        z = z.view((-1, 64, 6, 6))
        # z = F.max_unpool2d(z, idx4, (2, 2))
        # z = self.decode4(z)
        z = self.decode3(z)
        z = self.decode2(z)
        z = self.decode1(z)
        # z = z.view(-1, 128, 1, 1)
        # return self.decode_conv(z)
        return z

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return mu + eps * std

    def forward(self, x):
        mu, logvar = self.encode(x)
        z = self.reparameterize(mu, logvar)
        return self.decode(z), mu, logvar

    def loss(self, recon_x, x, mu, logvar):
        """https://github.com/pytorch/examples/blob/main/vae/main.py"""
        BCE = F.binary_cross_entropy(recon_x, x, reduction="sum")

        # see Appendix B from VAE paper:
        # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
        # https://arxiv.org/abs/1312.6114
        # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
        KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())

        return BCE + KLD


class BBoxTransform:
    squared: bool = False

    def __init__(self, squared: bool = None) -> None:
        if squared is not None:
            self.squared = squared

    def __call__(self, img: any) -> Tensor:
        img = transforms.F.to_tensor(img).squeeze(dim=0)
        ymin, ymax, xmin, xmax = bbox(img)
        if not self.squared:
            return transforms.F.to_pil_image(img[ymin:ymax, xmin:xmax].unsqueeze(dim=0))

        max_dim = (ymin, ymax) if ymax - ymin > xmax - xmin else (xmin, xmax)
        n = max_dim[1] - max_dim[0]
        if n % 2 != 0:
            n += 1

        n_med = np.round(n / 2)

        ymedian = np.round(ymin + (ymax - ymin) / 2)
        xmedian = np.round(xmin + (xmax - xmin) / 2)

        M, N = img.shape

        ycutmin, ycutmax = int(ymedian - n_med if ymedian >= n_med else 0), int(
            ymedian + n_med if ymedian + n_med <= M else M
        )

        xcutmin, xcutmax = int(xmedian - n_med if xmedian >= n_med else 0), int(
            xmedian + n_med if xmedian + n_med <= N else N,
        )

        if (ycutmax - ycutmin) % 2 != 0:
            ycutmin += 1
        if (xcutmax - xcutmin) % 2 != 0:
            xcutmin += 1

        squared_x = np.zeros((n, n))
        squared_cut_y = np.round((ycutmax - ycutmin) / 2)
        squared_cut_x = np.round((xcutmax - xcutmin) / 2)

        dest_ymin, dest_ymax = int(n_med - squared_cut_y), int(n_med + squared_cut_y)
        dest_xmin, dest_xmax = int(n_med - squared_cut_x), int(n_med + squared_cut_x)

        # print(ycutmin, ycutmax, ycutmax - ycutmin)
        # print(dest_ymin, dest_ymax, squared_cut_y + squared_cut_y)
        # print(xcutmin, xcutmax, xcutmax - xcutmin)
        # print(dest_xmin, dest_xmax, squared_cut_x + squared_cut_x)

        squared_x[
            dest_ymin:dest_ymax,
            dest_xmin:dest_xmax,
        ] = img[ycutmin:ycutmax, xcutmin:xcutmax]

        return transforms.F.to_pil_image(torch.from_numpy(squared_x).unsqueeze(dim=0))


class CloseTransform:
    kernel = torch.ones(5, 5)

    def __init__(self, kernel=None):
        if kernel is not None:
            self.kernel = kernel

    def __call__(self, x):
        x = transforms.F.to_tensor(x)

        if len(x.shape) < 4:
            return transforms.F.to_pil_image(
                closing(x.unsqueeze(dim=0), self.kernel).squeeze(dim=0)
            )

        return transforms.F.to_pil_image(closing(x, self.kernel))


class MPEG7ShapeDataset(Dataset):
    img_dir: str
    filenames: list[str] = []
    labels: list[str] = []
    label_dict: dict[str]
    transform: Transform = None

    def __init__(self, img_dir, transform=None):
        self.img_dir = img_dir
        self.transform = transform

        paths = os.listdir(self.img_dir)
        labels = []
        for file in paths:
            fp = os.path.join(self.img_dir, file)
            if os.path.isfile(fp):
                label = file.split("-")[0]
                self.filenames.append(fp)
                labels.append(label)

        label_name_dict = dict.fromkeys(labels)

        self.label_dict = {i: v for (i, v) in enumerate(label_name_dict.keys())}
        self.label_index_dict = {v: i for (i, v) in self.label_dict.items()}
        self.labels = [self.label_index_dict[l] for l in labels]

    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, idx):
        img_path = self.filenames[idx]
        gif = Image.open(img_path)
        gif.convert("RGB")
        label = self.labels[idx]
        if self.transform:
            image = self.transform(gif)
        return image, label


def load_mpeg7_data():
    transform = transforms.Compose(
        [
            transforms.Grayscale(),
            # transforms.RandomApply([CloseTransform()], p=0.25),
            BBoxTransform(squared=True),
            transforms.Resize(
                (64, 64), interpolation=transforms.InterpolationMode.NEAREST
            ),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.ToTensor(),
        ]
    )

    dataset = MPEG7ShapeDataset("shape_complexity/data/mpeg7", transform)

    data_loader = DataLoader(dataset, batch_size=128, shuffle=True)
    return data_loader, dataset


def load_dino_data():
    transform = transforms.Compose(
        [
            transforms.Grayscale(),
            # transforms.RandomApply([CloseTransform()], p=0.25),
            BBoxTransform(squared=True),
            transforms.Resize(
                (64, 64), interpolation=transforms.InterpolationMode.NEAREST
            ),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.ToTensor(),
        ]
    )

    dataset = ImageFolder("shape_complexity/data/dino", transform=transform)

    data_loader = DataLoader(dataset, batch_size=128, shuffle=True)
    return data_loader, dataset


def load_data():
    transform = transforms.Compose(
        [
            transforms.Grayscale(),
            transforms.RandomApply([CloseTransform()], p=0.25),
            transforms.Resize(
                (64, 64), interpolation=transforms.InterpolationMode.BILINEAR
            ),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.ToTensor(),
        ]
    )

    trajectories = (
        []
        if False
        else [
            # "v3_subtle_iceberg_lettuce_nymph-6_203-2056",
            "v3_absolute_grape_changeling-16_2277-4441",
            "v3_content_squash_angel-3_16074-17640",
            "v3_smooth_kale_loch_ness_monster-1_4439-6272",
            "v3_cute_breadfruit_spirit-6_17090-19102",
            "v3_key_nectarine_spirit-2_7081-9747",
            "v3_subtle_iceberg_lettuce_nymph-6_3819-6049",
            "v3_juvenile_apple_angel-30_396415-398113",
            "v3_subtle_iceberg_lettuce_nymph-6_6100-8068",
        ]
    )

    datasets = []
    for trj in trajectories:
        datasets.append(
            ImageFolder(
                f"activation_vis/out/critic/masks/{trj}/0/4", transform=transform
            )
        )

    datasets.append(
        ImageFolder("shape_complexity/data/simple_shapes", transform=transform)
    )

    dataset = torch.utils.data.ConcatDataset(datasets)

    data_loader = DataLoader(dataset, batch_size=128, shuffle=True)
    return data_loader, dataset


def train(epoch, model: VAE or CONVVAE, optimizer, data_loader, log_interval=40):
    model.train()
    train_loss = 0
    for batch_idx, (data, _) in enumerate(data_loader):
        data = data.to(device)

        optimizer.zero_grad()
        recon_batch, mu, logvar = model(data)
        loss = model.loss(recon_batch, data, mu, logvar)
        loss.backward()
        train_loss += loss.item()
        optimizer.step()

        if batch_idx % log_interval == 0:
            print(
                "Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}".format(
                    epoch,
                    batch_idx * len(data),
                    len(data_loader.dataset),
                    100.0 * batch_idx / len(data_loader),
                    loss.item() / len(data),
                )
            )

    print(
        "====> Epoch: {} Average loss: {:.4f}".format(
            epoch, train_loss / len(data_loader.dataset)
        )
    )


def test(epoch, models: list[CONVVAE] or list[VAE], dataset, save_results=False):
    for model in models:
        model.eval()
    test_loss = [0 for _ in models]

    test_batch_size = 32
    sampler = RandomSampler(dataset, replacement=True, num_samples=64)
    test_loader = DataLoader(dataset, batch_size=test_batch_size, sampler=sampler)
    comp_data = None

    with torch.no_grad():
        for i, (data, _) in enumerate(test_loader):
            data = data.to(device)

            for j, model in enumerate(models):
                recon_batch, mu, logvar = model(data)
                test_loss[j] += model.loss(recon_batch, data, mu, logvar).item()

                if i == 0:
                    n = min(data.size(0), 20)
                    if comp_data == None:
                        comp_data = data[:n]
                    comp_data = torch.cat(
                        [comp_data, recon_batch.view(test_batch_size, 1, 64, 64)[:n]]
                    )

            if i == 0 and save_results:
                if not os.path.exists("results"):
                    os.makedirs("results")
                save_image(
                    comp_data.cpu(),
                    "results/reconstruction_" + str(epoch) + ".png",
                    nrow=min(data.size(0), 20),
                )

    for i, model in enumerate(models):
        test_loss[i] /= len(test_loader.dataset)
        print(f"====> Test set loss model {model.bottleneck}: {test_loss[i]:.4f}")

    if save_results:
        plt.figure()
        bns = [m.bottleneck for m in models]
        plt.plot(bns, test_loss)
        plt.xticks(bns)
        plt.savefig("shape_complexity/results/vae_graph.png")
        plt.clf()


def l2_distance_measure(img: Tensor, model: VAE):
    model.eval()

    with torch.no_grad():
        mask = img.to(device)
        recon, mean, _ = model(mask)
        # TODO: apply threshold here?!
        _, recon_mean, _ = model(recon)

        distance = torch.norm(mean - recon_mean, p=2)

        return (
            distance,
            make_grid(torch.stack([mask[0], recon[0]]).cpu(), nrow=2, padding=0),
        )


def compression_measure(img: Tensor, fill_ratio_norm=False):
    np_img = img[0].numpy()
    compressed = compress(np_img)

    if fill_ratio_norm:
        fill_ratio = np_img.sum().item() / np.ones_like(np_img).sum().item()
        return len(compressed) * (1 - fill_ratio), None

    return len(compressed), None


def fft_measure(img: Tensor):
    np_img = img[0][0].numpy()
    fft = np.fft.fft2(np_img)

    fft_abs = np.abs(fft)

    n = fft.shape[0]
    pos_f_idx = n // 2
    df = np.fft.fftfreq(n=n)

    amplitude_sum = fft_abs[:pos_f_idx, :pos_f_idx].sum()
    mean_x_freq = (fft_abs * df)[:pos_f_idx, :pos_f_idx].sum() / amplitude_sum
    mean_y_freq = (fft_abs.T * df).T[:pos_f_idx, :pos_f_idx].sum() / amplitude_sum

    mean_freq = np.sqrt(np.power(mean_x_freq, 2) + np.power(mean_y_freq, 2))

    # mean frequency in range 0 to 0.5
    return mean_freq / 0.5, None


def pixelwise_complexity_measure(
    img: Tensor,
    model_gb: nn.Module,
    model_lb: nn.Module,
    fill_ratio_norm=False,
):
    model_gb.eval()
    model_lb.eval()

    with torch.no_grad():
        mask = img.to(device)

        recon_gb: Tensor
        recon_lb: Tensor

        recon_gb, _, _ = model_gb(mask)
        recon_lb, _, _ = model_lb(mask)

        max_px_fill = torch.ones_like(mask).sum().item()
        abs_px_diff = (recon_gb - recon_lb).abs().sum().item()

        complexity = abs_px_diff / max_px_fill

        # this equals complexity = (1 - fill_rate) * diff_px / max_px
        if fill_ratio_norm:
            complexity -= abs_px_diff * mask.sum().item() / np.power(max_px_fill, 2)
            # complexity *= mask.sum().item() / max_px_fill

        return (
            complexity,
            make_grid(
                torch.stack(
                    [mask[0], recon_lb.view(-1, 64, 64), recon_gb.view(-1, 64, 64)]
                ).cpu(),
                nrow=3,
                padding=0,
            ),
        )


def complexity_measure(
    img: Tensor,
    model_gb: nn.Module,
    model_lb: nn.Module,
    epsilon=0.4,
    fill_ratio_norm=False,
):
    model_gb.eval()
    model_lb.eval()

    with torch.no_grad():
        mask = img.to(device)

        recon_gb, _, _ = model_gb(mask)
        recon_lb, _, _ = model_lb(mask)

        recon_bits_gb = recon_gb.view(-1, 64, 64).cpu() > epsilon
        recon_bits_lb = recon_lb.view(-1, 64, 64).cpu() > epsilon
        mask_bits = mask[0].cpu() > 0

        tp_gb = (mask_bits & recon_bits_gb).sum()
        fp_gb = (recon_bits_gb & ~mask_bits).sum()
        tp_lb = (mask_bits & recon_bits_lb).sum()
        fp_lb = (recon_bits_lb & ~mask_bits).sum()

        prec_gb = tp_gb / (tp_gb + fp_gb)
        prec_lb = tp_lb / (tp_lb + fp_lb)
        prec_gb = 0 if torch.isnan(prec_gb) else prec_gb
        prec_lb = 0 if torch.isnan(prec_lb) else prec_lb

        complexity = 1 - (prec_gb - np.abs(prec_gb - prec_lb))

        if fill_ratio_norm:
            fill_ratio = mask.sum().item() / torch.ones_like(img).sum().item()
            complexity *= 1 - fill_ratio

        return (
            complexity,
            make_grid(
                torch.stack(
                    [mask[0], recon_lb.view(-1, 64, 64), recon_gb.view(-1, 64, 64)]
                ).cpu(),
                nrow=3,
                padding=0,
            ),
        )


def mean_precision(img: Tensor, models: list[nn.Module], epsilon=0.4):
    mask = img.to(device)
    mask_bits = mask[0].cpu() > 0

    precisions = np.zeros(len(models))

    for i, model in enumerate(models):
        recon_gb, _, _ = model(mask)

        recon_bits = recon_gb.view(-1, 64, 64).cpu() > epsilon

        tp = (mask_bits & recon_bits).sum()
        fp = (recon_bits & ~mask_bits).sum()

        prec = tp / (tp + fp)
        precisions[i] = prec

    return 1 - precisions.mean(), None


def complexity_measure_diff(
    img: Tensor,
    model_gb: nn.Module,
    model_lb: nn.Module,
):
    model_gb.eval()
    model_lb.eval()

    with torch.no_grad():
        mask = img.to(device)
        recon_gb, _, _ = model_gb(mask)
        recon_lb, _, _ = model_lb(mask)

        diff = torch.abs((recon_gb - recon_lb).cpu().sum())

        return (
            diff,
            make_grid(
                torch.stack(
                    [mask[0], recon_lb.view(-1, 64, 64), recon_gb.view(-1, 64, 64)]
                ).cpu(),
                nrow=3,
                padding=0,
            ),
        )


def plot_samples(masks: Tensor, ratings: npt.NDArray):
    dpi = 150
    rows = cols = 20
    total = rows * cols
    n_samples, _, y, x = masks.shape

    extent = (0, x - 1, 0, y - 1)

    if total != n_samples:
        raise Exception("shape mismatch")

    fig = plt.figure(figsize=(32, 16), dpi=dpi)
    for idx in np.arange(n_samples):
        ax = fig.add_subplot(rows, cols, idx + 1, xticks=[], yticks=[])

        plt.imshow(masks[idx][0], cmap=plt.cm.gray, extent=extent)
        ax.set_title(
            f"{ratings[idx]:.4f}",
            fontdict={"fontsize": 6, "color": "orange"},
            y=0.35,
        )

    fig.patch.set_facecolor("#292929")
    height_px = y * rows
    width_px = x * cols
    fig.set_size_inches(width_px / (dpi / 2), height_px / (dpi / 2), forward=True)
    fig.tight_layout(pad=0)

    return fig


def visualize_sort(
    data_loader: DataLoader,
    metric_fn: Callable[[Tensor, any], tuple[torch.float32, Tensor]],
    metric_name: str,
    *fn_args: any,
    plot_ratings=False,
    **fn_kwargs: any,
):
    recon_masks = None
    masks = torch.zeros((400, 1, 64, 64))
    ratings = torch.zeros((400,))

    plot_recons = True

    for i, (mask, _) in enumerate(data_loader, 0):
        rating, mask_recon_grid = metric_fn(mask, *fn_args, **fn_kwargs)
        if plot_recons and mask_recon_grid == None:
            plot_recons = False
        elif plot_recons and recon_masks is None:
            recon_masks = torch.zeros((400, *mask_recon_grid.shape))

        masks[i] = mask[0]
        ratings[i] = rating

        if plot_recons:
            recon_masks[i] = mask_recon_grid

    if plot_ratings:
        plt.plot(np.arange(len(ratings)), np.sort(ratings.numpy()))
        plt.xlabel("images")
        plt.ylabel(f"{metric_name} rating")
        plt.savefig(f"shape_complexity/results/{metric_name}_rating_plot.png")
        plt.close()

    sort_idx = torch.argsort(ratings)

    masks_sorted = masks.numpy()[sort_idx]
    fig = plot_samples(masks_sorted, ratings.numpy()[sort_idx])
    fig.savefig(f"shape_complexity/results/{metric_name}_sort.png")
    plt.close(fig)

    if plot_recons:
        recon_masks_sorted = recon_masks.numpy()[sort_idx]
        fig_recon = plot_samples(recon_masks_sorted, ratings.numpy()[sort_idx])
        fig_recon.savefig(f"shape_complexity/results/{metric_name}_sort_recon.png")
        plt.close(fig_recon)


def visualize_sort_multidim(
    data_loader: DataLoader,
    measures: list[
        tuple[str, Callable[[Tensor, any], tuple[torch.float32, Tensor]], any]
    ],
    max_norm=False,
):
    n_dim = len(measures)
    masks = torch.zeros((400, 1, 64, 64))
    ratings = torch.zeros((400, n_dim))

    for i, (mask, _) in enumerate(data_loader, 0):
        masks[i] = mask[0]

        for m in range(n_dim):
            name, fn, args = measures[m]
            rating, _ = fn(mask, *args)
            ratings[i, m] = rating

    # TODO: generate plot if n_dim is > 1 and < 4

    if max_norm:
        maxs = ratings.max(dim=0)
        ratings[:] /= maxs.values

    measure_norm = torch.linalg.vector_norm(ratings, dim=1)
    sort_idx = np.argsort(np.array(measure_norm))

    fig = plot_samples(masks.numpy()[sort_idx], measure_norm.numpy()[sort_idx])
    fig.savefig(
        f"shape_complexity/results/{n_dim}dim_{'_'.join([m[0] for m in measures])}_sort.png"
    )
    plt.close(fig)


def visualize_sort_3dim(
    data_loader: DataLoader, model_gb: nn.Module, model_lb: nn.Module
):
    masks_recon = torch.zeros((400, 3, 64, 192))
    masks = torch.zeros((400, 1, 64, 64))
    measures = torch.zeros((400, 3))
    for i, (mask, _) in enumerate(data_loader, 0):
        c_compress, _ = compression_measure(mask, fill_ratio_norm=True)
        c_fft, _ = fft_measure(mask)
        c_vae, mask_recon_grid = complexity_measure(
            mask, model_gb, model_lb, fill_ratio_norm=True
        )
        masks_recon[i] = mask_recon_grid
        masks[i] = mask[0]
        measures[i] = torch.tensor([c_compress, c_fft, c_vae])

    measures[:] /= measures.max(dim=0).values
    measure_norm = torch.linalg.vector_norm(measures, dim=1)

    fig = plt.figure()
    fig.clf()
    ax = fig.add_subplot(projection="3d")
    ax.scatter(measures[:, 0], measures[:, 1], measures[:, 2], marker="o")

    ax.set_xlabel("zlib compression")
    ax.set_ylabel("FFT ratio")
    ax.set_zlabel(f"VAE ratio {model_gb.bottleneck}/{model_lb.bottleneck}")
    plt.savefig("shape_complexity/results/3d_plot.png")
    plt.close()

    sort_idx = np.argsort(np.array(measure_norm))
    recon_masks_sorted = masks_recon.numpy()[sort_idx]
    masks_sorted = masks.numpy()[sort_idx]

    return (
        plot_samples(masks_sorted, measure_norm[sort_idx]),
        plot_samples(recon_masks_sorted, measure_norm[sort_idx]),
    )


LR = 1.5e-3
EPOCHS = 80
LOAD_PRETRAINED = False


def main():
    bottlenecks = [4, 8, 16, 32]
    models = {i: CONVVAE(bottleneck=i).to(device) for i in bottlenecks}
    optimizers = {i: Adam(model.parameters(), lr=LR) for i, model in models.items()}

    # data_loader, dataset = load_data()
    data_loader, dataset = load_mpeg7_data()
    data_loader, dataset = load_dino_data()

    train_size = int(0.8 * len(dataset))
    test_size = len(dataset) - train_size
    train_dataset, test_dataset = torch.utils.data.random_split(
        dataset, [train_size, test_size]
    )
    train_loader = DataLoader(train_dataset, batch_size=256, shuffle=True)

    if LOAD_PRETRAINED:
        for i, model in models.items():
            model.load_state_dict(
                torch.load(f"shape_complexity/trained/CONVVAE_{i}_split_data.pth")
            )
    else:
        for epoch in range(EPOCHS):
            for i, model in models.items():
                train(
                    epoch,
                    model=model,
                    optimizer=optimizers[i],
                    data_loader=train_loader,
                )

            test(epoch, models=list(models.values()), dataset=test_dataset)

        for bn in bottlenecks:
            if not os.path.exists("shape_complexity/trained"):
                os.makedirs("shape_complexity/trained")

            torch.save(
                models[bn].state_dict(),
                f"shape_complexity/trained/CONVVAE_{bn}_split_data.pth",
            )

    test(0, models=list(models.values()), dataset=test_dataset, save_results=True)

    bn_gt = 32
    bn_lt = 8

    sampler = RandomSampler(dataset, replacement=True, num_samples=400)
    data_loader = DataLoader(dataset, batch_size=1, sampler=sampler)

    visualize_sort_multidim(
        data_loader,
        [
            (
                "recon_comp",
                complexity_measure,
                [models[bn_gt], models[bn_lt], 0.4, True],
            ),
            ("fft", fft_measure, []),
            ("compression", compression_measure, [True]),
        ],
        max_norm=True,
    )
    visualize_sort_multidim(
        data_loader,
        [
            (
                "px_recon_comp",
                pixelwise_complexity_measure,
                [models[bn_gt], models[bn_lt], True],
            ),
            ("fft", fft_measure, []),
            ("compression", compression_measure, [True]),
        ],
        max_norm=True,
    )
    visualize_sort_multidim(
        data_loader,
        [
            ("fft", fft_measure, []),
            ("compression", compression_measure, [True]),
        ],
        max_norm=True,
    )
    visualize_sort_multidim(
        data_loader,
        [
            (
                "recon_comp",
                complexity_measure,
                [models[bn_gt], models[bn_lt], 0.4, True],
            ),
            ("fft", fft_measure, []),
        ],
        max_norm=True,
    )
    visualize_sort_multidim(
        data_loader,
        [
            (
                "recon_comp",
                complexity_measure,
                [models[bn_gt], models[bn_lt], 0.4, True],
            ),
            ("compression", compression_measure, [True]),
        ],
        max_norm=True,
    )

    visualize_sort(
        data_loader,
        complexity_measure,
        "recon_complexity",
        models[bn_gt],
        models[bn_lt],
        fill_ratio_norm=True,
    )
    visualize_sort(
        data_loader,
        pixelwise_complexity_measure,
        "px_recon_complexity",
        models[bn_gt],
        models[bn_lt],
        fill_ratio_norm=True,
    )

    visualize_sort(data_loader, mean_precision, "mean_precision", list(models.values()))
    visualize_sort(data_loader, fft_measure, "fft")
    visualize_sort(data_loader, compression_measure, "compression")
    visualize_sort(
        data_loader,
        compression_measure,
        "compression_fill_norm",
        fill_ratio_norm=True,
    )
    visualize_sort(
        data_loader, l2_distance_measure, "latent_l2_distance", models[bn_gt]
    )


if __name__ == "__main__":
    main()
