import os

import cv2 as cv
import numpy as np

rng = np.random.default_rng()

IMAGE_EXTENT = (64, 64)
BLACK = 0
WHITE = 255


def draw_random_rectangle(image):
    p0 = np.random.randint(0, 64, size=(2,))
    p1 = np.random.randint(0, 64, size=(2,))
    cv.rectangle(
        image,
        p0,
        p1,
        WHITE,
        cv.FILLED,
    )

    return image


def draw_random_ellipse(image):
    axes = np.random.randint(0, 31, size=(2,))
    angle = np.random.randint(0, 180)
    cv.ellipse(image, (31, 31), axes, angle, 0, 360, WHITE, cv.FILLED)

    return image


def draw_random_sized_star(image):
    pts = [
        (0, 3.5),
        (1, 1.5),
        (3.5, 1.5),
        (1.5, -0.5),
        (2.5, -2.5),
        (0, -1.5),
        (-2.5, -2.5),
        (-1.5, -0.5),
        (-3.5, 1.5),
        (-1, 1.5),
    ]

    rot_matrix = cv.getRotationMatrix2D((0, 0), angle=np.random.random() * 360, scale=1)

    np_pts = np.array([pts])

    pts_rot_scaled = np.round(
        cv.transform(np_pts, rot_matrix) * np.random.rand() * 8 + 31
    ).astype(int)

    cv.fillPoly(image, pts_rot_scaled, WHITE)

    return image


def draw_random_triangle(image):
    pts = np.random.randint(0, 64, size=(3, 2))
    cv.fillPoly(image, np.array([pts]), WHITE)

    return image


N_SHAPES = 3000
DATA_PATH = "shape_complexity/data/simple_shapes/0"


def bbox(img):
    rows = np.any(img, axis=1)
    cols = np.any(img, axis=0)
    rmin, rmax = np.where(rows)[0][[0, -1]]
    cmin, cmax = np.where(cols)[0][[0, -1]]

    rmin = np.max((0, rmin - 10))
    cmin = np.max((0, cmin - 10))
    rmax = np.min((63, rmax + 10))
    cmax = np.min((63, cmax + 10))

    return img[rmin:rmax, cmin:cmax]


def main():
    if not os.path.exists(DATA_PATH):
        os.makedirs(DATA_PATH)

    for i in range(N_SHAPES):
        img = np.zeros(IMAGE_EXTENT, np.uint8)

        shape_type = np.random.randint(0, 4)
        if shape_type == 0:
            draw_random_ellipse(img)
        elif shape_type == 1:
            draw_random_rectangle(img)
        elif shape_type == 2:
            draw_random_sized_star(img)
        else:
            draw_random_triangle(img)

        img = bbox(img)

        cv.imwrite(f"{DATA_PATH}/{i}.png", img)


if __name__ == "__main__":
    main()
