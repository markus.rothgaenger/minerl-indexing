import imageio
from imageio import v3 as iio
import os
import numpy as np
import numpy.typing as npt
import sys


# reference: https://stackoverflow.com/questions/14465297/connected-component-labeling-implementation
# perform depth first search for each candidate/unlabeled region
dx = [+1, 0, -1, 0]
dy = [0, +1, 0, -1]


def dfs(mask: npt.NDArray, x: int, y: int, labels: npt.NDArray, current_label: int):
    n_rows, n_cols = mask.shape
    if x < 0 or x == n_rows:
        return
    if y < 0 or y == n_cols:
        return
    if labels[x][y] or not mask[x][y]:
        return  # already labeled or not marked with 1 in image

    # mark the current cell
    labels[x][y] = current_label

    # recursively mark the neighbors
    for direction in range(4):
        dfs(mask, x + dx[direction], y + dy[direction], labels, current_label)


def find_components(mask: npt.NDArray):
    label = 0

    n_rows, n_cols = mask.shape
    labels = np.zeros(mask.shape, dtype=np.int8)

    for i in range(n_rows):
        for j in range(n_cols):
            if not labels[i][j] and mask[i][j]:
                label += 1
                dfs(mask, i, j, labels, label)

    return labels


# https://stackoverflow.com/questions/31400769/bounding-box-of-numpy-array
def bbox(img):
    rows = np.any(img, axis=1)
    cols = np.any(img, axis=0)
    rmin, rmax = np.where(rows)[0][[0, -1]]
    cmin, cmax = np.where(cols)[0][[0, -1]]

    return rmin, rmax, cmin, cmax


def extract_single_masks(labels: npt.NDArray):
    masks = []
    for l in range(1, labels.max() + 1):
        # TODO: ignor label 0 ????
        mask = (labels == l).astype(np.int8)
        max_x, max_y = mask.shape
        pixel_sum = mask.sum()
        if pixel_sum > 128 and pixel_sum < 512:
            # print(pixel_sum)
            rmin, rmax, cmin, cmax = bbox(mask)
            rmin = rmin - 1 if rmin > 0 else rmin
            cmin = cmin - 1 if cmin > 0 else cmin
            rmax = rmax + 1 if rmax < max_x else rmax
            cmax = cmax + 1 if cmax < max_y else cmax

            aspect_ratio = (rmax - rmin) / (cmax - cmin)
            if aspect_ratio > 0.5 and aspect_ratio < 2.0:
                masks.append(mask[rmin : rmax + 1, cmin : cmax + 1])
            else:
                pass
                # print("rejected due to aspect ratio")

    return masks


clusters = 4
total = 0

trajectories = [
    "v3_subtle_iceberg_lettuce_nymph-6_203-2056",
    "v3_absolute_grape_changeling-16_2277-4441",
    "v3_content_squash_angel-3_16074-17640",
    "v3_smooth_kale_loch_ness_monster-1_4439-6272",
    "v3_cute_breadfruit_spirit-6_17090-19102",
    "v3_key_nectarine_spirit-2_7081-9747",
    "v3_subtle_iceberg_lettuce_nymph-6_3819-6049",
    "v3_juvenile_apple_angel-30_396415-398113",
    "v3_subtle_iceberg_lettuce_nymph-6_6100-8068",
]

sys.setrecursionlimit(3000)

for trj in trajectories:
    print(trj)
    path = f"activation_vis/out/critic/masks/{trj}/0/{clusters}"
    for i in range(clusters):
        fp = os.path.join(path, str(i))
        if not os.path.exists(fp):
            os.makedirs(fp)

    for file in os.listdir(path):
        fullpath = os.path.join(path, file)
        if os.path.isdir(fullpath):
            continue

        image = imageio.imread(fullpath)
        image = (image / (255 / (clusters - 1))).astype(np.int8)

        for i in range(clusters):
            cluster_image = (image == i).astype(np.int8)

            labels = find_components(cluster_image)
            single_masks = extract_single_masks(labels)
            for l, mask in enumerate(single_masks):
                total += 1
                imageio.imwrite(
                    os.path.join(path, str(i), f"{file.replace('.png', '')}_{l}.png"),
                    (mask * 255).astype(np.uint8),
                )

print(f"total masks: {total}")
