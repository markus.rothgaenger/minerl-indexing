from asyncio import subprocess
import itertools
import logging
import os
from multiprocessing import Pool, set_start_method, log_to_stderr
from multiprocessing.dummy import Pool as ThreadPool
from subprocess import call

import faiss
import imageio
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import minerl
import numpy as np
import torch
import torchvision.transforms as T
from matplotlib.backends.backend_agg import FigureCanvasAgg
from minerl.data import BufferedBatchIter
from numpy import typing as npt
from torch import Tensor, nn

# eset_start_method("fork")
os.environ["MINERL_DATA_ROOT"] = f"/home/{os.environ.get('USER')}/minerl"
logging.basicConfig(level=logging.ERROR)
rng = np.random.default_rng()
data = minerl.data.make("MineRLTreechop-v0")

RGB_INDEX = -1
POOL_SIZE = 8
MAX_FRAMES = 500

class MobileNetV2(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = torch.hub.load(
            "pytorch/vision:v0.11.1", "mobilenet_v2", pretrained=True
        ).eval()
        self.transforms = nn.Sequential(
            T.Resize([224, 224]),
            T.ConvertImageDtype(torch.float),
            T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
        )
        # if torch.cuda.is_available():
        #     print('cuda enabled!')
        #     self.model.to('cuda')

    def eval(self, x: Tensor) -> Tensor:
        with torch.no_grad():
            x = x.permute(0, 3, 1, 2)
            x = self.transforms(x)
            # x = x.unsqueeze(0)
            # if torch.cuda.is_available():
            #     x = x.to('cuda')
            return self.model(x)

    def eval_intermediate(self, x: Tensor) -> tuple[Tensor, list[Tensor]]:
        with torch.no_grad():
            x = x.permute(0, 3, 1, 2)
            results = []
            x = self.transforms(x)            

            for layer in self.model.features:
                x = layer(x)
                _mi = x.min()
                _ma = x.max()
                print(_mi)
                print(_ma)
                results.append(x)

            x = nn.functional.adaptive_avg_pool2d(x, (1, 1))            
            x = torch.flatten(x, 1)            
            x = self.model.classifier(x)            

            return x, results


class VGG11(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = torch.hub.load(
            "pytorch/vision:v0.11.1", "vgg11", pretrained=True
        ).eval()
        self.transforms = nn.Sequential(
            T.Resize([224, 224]),
            T.ConvertImageDtype(torch.float),
            T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
        )
        # if torch.cuda.is_available():
        #     print('cuda enabled!')
        #     self.model.to('cuda')

    def eval(self, x: Tensor) -> Tensor:
        with torch.no_grad():
            x = x.permute(0, 3, 1, 2)
            x = self.transforms(x)
            # x = x.unsqueeze(0)
            # if torch.cuda.is_available():
            #     x = x.to('cuda')
            return self.model(x)

    def eval_intermediate(self, x: Tensor) -> tuple[Tensor, list[Tensor]]:
        with torch.no_grad():
            x = x.permute(0, 3, 1, 2)
            results = []
            x = self.transforms(x)
            # results.append(x)
            # x = x.unsqueeze(0)
            # results.append(x)

            for layer in self.model.features:
                x = layer(x)
                results.append(x)

            x = self.model.avgpool(x)
            x = torch.flatten(x, 1)
            x = self.model.classifier(x)

            return x, results


class NewCritic(nn.Module):
    def __init__(
        self,
        width=64,
        dims=[8, 8, 8, 16],
        bottleneck=32,
        colorchs=3,
        chfak=1,
        activation=nn.ReLU,
        pool="max",
        dropout=0.5,
    ):
        super().__init__()
        self.width = width
        stride = 1 if pool == "max" else 2
        dims = np.array(dims) * chfak
        pool = nn.MaxPool2d(2) if pool == "max" else nn.Identity()
        self.pool = pool
        features = [
            nn.Conv2d(colorchs, dims[0], 3, stride, 1),
            activation(),
            pool,
            nn.Conv2d(dims[0], dims[1], 3, stride, 1),
            activation(),
            pool,
            nn.Conv2d(dims[1], dims[2], 3, stride, 1),
            activation(),
            pool,
            nn.Dropout(dropout),
            nn.Conv2d(dims[2], dims[3], 3, stride, 1),
            activation(),
            pool,
            nn.Dropout(dropout),
            nn.Conv2d(dims[3], bottleneck * chfak, 4),
            activation(),
        ]
        self.features = nn.Sequential(*features)

        self.crit = nn.Sequential(
            nn.Flatten(),
            nn.Linear(chfak * bottleneck, chfak * bottleneck),
            activation(),
            nn.Dropout(dropout),
            nn.Linear(chfak * bottleneck, 1),
            nn.Sigmoid(),
        )

    def forward(self, X, collect=False):
        embeds = []
        # print(list(self.features))
        for layer in list(self.features):
            X = layer(X)
            if collect and isinstance(layer, type(self.pool)):
                embeds.append(X)
        if collect:
            embeds.append(X)
        # print("last embed", X.shape)
        pred = self.crit(X)

        if collect:
            return pred, embeds
        else:
            return pred

    def preprocess(self, X: Tensor):
        # X = X.T.unsqueeze(0)
        return (X / 255.0).permute(0, 3, 1, 2).float()
        # return (X/255.0).float()

    def eval_intermediate(self, X):
        with torch.no_grad():
            X = self.preprocess(X)
            return self.forward(X, collect=True)


def prettySize(s: torch.Size):
    # return '-'.join([str(e) for e in s])
    return tuple([e for e in s])


def train_cluster_models(
    data, model, trajectories: list[str], n_clusters=7, batch_size=32, n_batches=1
) -> dict[int, tuple[faiss.Kmeans, faiss.Kmeans]]:
    layer_outputs: dict[int, npt.NDArray] = {}
    kmeans_models: dict[int, tuple[faiss.Kmeans, faiss.Kmeans]] = {}

    for n_trajectory, trajectory in enumerate(trajectories):
        iterator = BufferedBatchIter(data)
        iterator.available_trajectories = [trajectory]
        n_batch = 0

        for current_state, _, _, _, _ in iterator.buffered_batch_iter(
            batch_size=batch_size, num_batches=n_batches
        ):
            _, intermediate_act = model.eval_intermediate(Tensor(current_state["pov"]))

            idx_begin = n_trajectory * n_batches * batch_size + n_batch * batch_size
            _pov = current_state["pov"]

            for i in range(len(intermediate_act)):
                _act = intermediate_act[i].permute(0, 2, 3, 1)

                if layer_outputs.get(i) is None:
                    layer_outputs[i] = np.zeros(
                        (len(trajectories) * batch_size * n_batches, *_act[0].shape),
                        dtype=np.float32,
                    )

                layer_outputs[i][idx_begin : idx_begin + batch_size] = _act

            if layer_outputs.get(RGB_INDEX) is None:
                layer_outputs[RGB_INDEX] = np.zeros(
                    (len(trajectories) * batch_size * n_batches, *_pov[0].shape),
                    dtype=np.float32,
                )
            layer_outputs[RGB_INDEX][idx_begin : idx_begin + batch_size] = _pov

        n_batch += 1

    for k, v in layer_outputs.items():
        _shape = v.shape
        data = v.reshape((-1, _shape[3]))
        _, n_channels = data.shape

        kmeans = faiss.Kmeans(n_channels, n_clusters)
        kmeansX2 = faiss.Kmeans(n_channels, n_clusters * 2 + 1)
        kmeans.train(data)
        kmeansX2.train(data)

        kmeans_models[k] = (kmeans, kmeansX2)

    return kmeans_models


def generate_plot_buffer(
    batch_size, intermediate_act, k_map, c_values, c_valuesX2, pov
):
    extent = 0, 63, 0, 63
    ref_axes = None
    act_axes = None
    actX2_axes = None
    rgb_ref_ax = None
    rgb_act_ax = None
    rgb_actX2_ax = None

    fig = plt.figure(figsize=(13, 2), dpi=200)
    canvas = FigureCanvasAgg(fig)
    fig_setup_complete = False

    n_layers = len(intermediate_act)
    n_vis = n_layers + 1

    results = []

    for i in range(batch_size):
        if not fig_setup_complete:
            ref_axes = {_i: plt.subplot(3, n_vis, _i + 2) for _i in range(n_layers)}
            act_axes = {
                _i: plt.subplot(3, n_vis, _i + n_vis + 2) for _i in range(n_layers)
            }
            actX2_axes = {
                _i: plt.subplot(3, n_vis, _i + 2 * n_vis + 2) for _i in range(n_layers)
            }
            rgb_ref_ax = plt.subplot(3, n_vis, 1)
            rgb_act_ax = plt.subplot(3, n_vis, n_vis + 1)
            rgb_actX2_ax = plt.subplot(3, n_vis, 2 * n_vis + 1)

        for _, v in ref_axes.items():
            v.clear()
            v.axis("off")
        for _, v in act_axes.items():
            v.clear()
            v.axis("off")
        for _, v in actX2_axes.items():
            v.clear()
            v.axis("off")
        rgb_ref_ax.clear()
        rgb_ref_ax.axis("off")
        rgb_act_ax.clear()
        rgb_act_ax.axis("off")
        rgb_actX2_ax.clear()
        rgb_actX2_ax.axis("off")

        # write
        for layer, _act in enumerate(intermediate_act):
            _act = _act[i]

            ref_ax = ref_axes[layer]
            act_ax = act_axes[layer]
            actX2_ax = actX2_axes[layer]

            k, k_X2 = k_map[layer]

            cmap = cm.get_cmap("gist_rainbow", k)
            cmapX2 = cm.get_cmap("gist_rainbow", k_X2)
            ref_ax.imshow(pov[i], extent=extent)
            act_ax.imshow(pov[i], alpha=1.0, extent=extent)            
            act_ax.imshow(
                c_values[layer][i], cmap=cmap, alpha=0.5, extent=extent, vmin=0, vmax=k
            )

            actX2_ax.imshow(pov[i], alpha=1.0, extent=extent)            
            actX2_ax.imshow(
                c_valuesX2[layer][i],
                cmap=cmapX2,
                alpha=0.5,
                extent=extent,
                vmin=0,
                vmax=k_X2,
            )
            ref_ax.set_title(
                f"n_c: {k}/{k_X2}\n {prettySize(_act.shape)}", fontdict={"fontsize": 6}
            )

        # RGB stuff
        k, k_X2 = k_map[RGB_INDEX]
        cmap = cm.get_cmap("gist_rainbow", k)
        cmapX2 = cm.get_cmap("gist_rainbow", k_X2)
        rgb_ref_ax.imshow(pov[i], extent=extent)
        rgb_act_ax.imshow(pov[i], alpha=1.0, extent=extent)
        rgb_act_ax.imshow(
            c_values[RGB_INDEX][i], cmap=cmap, alpha=0.5, extent=extent, vmin=0, vmax=k
        )
        rgb_actX2_ax.imshow(pov[i], alpha=1.0, extent=extent)
        rgb_actX2_ax.imshow(
            c_valuesX2[RGB_INDEX][i],
            cmap=cmapX2,
            alpha=0.5,
            extent=extent,
            vmin=0,
            vmax=k,
        )
        rgb_ref_ax.set_title(
            f"RGB n_c: {k}/{k_X2}\n {prettySize(pov[i].shape)}",
            fontdict={"fontsize": 6},
        )

        if not fig_setup_complete:
            fig.tight_layout(pad=0.1)
            fig_setup_complete = True

        canvas.draw()
        s, (width, height) = canvas.print_to_buffer()

        results.append(np.frombuffer(s, np.uint8).reshape((height, width, 4)))

    return results


def visualize_clusters_video(
    data,
    model,
    trajectory: str,
    kmeans_models: dict[int, tuple[faiss.Kmeans, faiss.Kmeans]],
    out_dir: str,
    batch_size=64,
):
    n_frames = 0
    finish_video = False

    if not os.path.exists(out_dir):
        os.mkdirs(out_dir)

    with imageio.get_writer(f"{out_dir}/{trajectory}.mp4", mode="I", fps=25) as writer:
        iterator = BufferedBatchIter(data)
        # this works around getting batches for random trajectories :)
        iterator.available_trajectories = [trajectory]

        for current_state, _, _, _, _ in iterator.buffered_batch_iter(
            batch_size=batch_size, num_epochs=1
        ):
            _, intermediate_act = model.eval_intermediate(Tensor(current_state["pov"]))

            pov = current_state["pov"]

            k_map = {}
            c_values = {}
            c_valuesX2 = {}

            # cluster
            for layer, _act in enumerate(intermediate_act):
                _act = _act.permute(0, 2, 3, 1)

                kmeans, kmeansX2 = kmeans_models[layer]
                k_map[layer] = (kmeans.k, kmeansX2.k)

                _, I = kmeans.index.search(
                    np.array(_act, dtype=np.float32).reshape((-1, _act.shape[-1])),
                    1,
                )
                c_values[layer] = I.reshape((*_act.shape[0:3], 1))

                _, I = kmeansX2.index.search(
                    np.array(_act, dtype=np.float32).reshape((-1, _act.shape[-1])),
                    1,
                )
                c_valuesX2[layer] = I.reshape((*_act.shape[0:3], 1))

            kmeans, kmeansX2 = kmeans_models[RGB_INDEX]
            k_map[RGB_INDEX] = (kmeans.k, kmeansX2.k)

            _, I = kmeans.index.search(
                np.array(pov, dtype=np.float32).reshape((-1, pov.shape[-1])), 1
            )
            c_values[RGB_INDEX] = I.reshape((*pov.shape[0:3], 1))

            _, I = kmeansX2.index.search(
                np.array(pov, dtype=np.float32).reshape((-1, pov.shape[-1])), 1
            )
            c_valuesX2[RGB_INDEX] = I.reshape((*pov.shape[0:3], 1))

            _batch_size = int(batch_size / POOL_SIZE)
            batch_idx = np.arange(0, batch_size, _batch_size)

            with Pool(POOL_SIZE) as pool:
                args = zip(
                    itertools.repeat(_batch_size),
                    [
                        [
                            intermediate_act[i][s : s + _batch_size]
                            for i in range(len(intermediate_act))
                        ]
                        for s in batch_idx
                    ],
                    itertools.repeat(k_map),
                    [
                        {
                            i: c_values[i][s : s + _batch_size]
                            for i in range(-1, len(intermediate_act))
                        }
                        for s in batch_idx
                    ],
                    [
                        {
                            i: c_valuesX2[i][s : s + _batch_size]
                            for i in range(-1, len(intermediate_act))
                        }
                        for s in batch_idx
                    ],
                    [pov[s : s + _batch_size] for s in batch_idx],
                )

                results = pool.starmap(generate_plot_buffer, args)

                pool.close()
                pool.join()

            for b in results:
                for img in b:
                    n_frames += 1
                    writer.append_data(img)

            print(f"{trajectory}: frame {n_frames}", end="\r")

            if n_frames > MAX_FRAMES:
                finish_video = True
                break

            if finish_video:
                break

    plt.close()


if __name__ == "__main__":
    trajectories = [
        "v3_svelte_cherry_devil-12_30091-31945",
        "v3_subtle_iceberg_lettuce_nymph-6_203-2056",
        "v3_absolute_grape_changeling-16_2277-4441",
        "v3_content_squash_angel-3_16074-17640",
        "v3_smooth_kale_loch_ness_monster-1_4439-6272",
        "v3_cute_breadfruit_spirit-6_17090-19102",
        "v3_key_nectarine_spirit-2_7081-9747",
        "v3_subtle_iceberg_lettuce_nymph-6_3819-6049",
        "v3_juvenile_apple_angel-30_396415-398113",
        "v3_subtle_iceberg_lettuce_nymph-6_6100-8068",
    ]

    if True:
        mn = MobileNetV2()
        models_mn = train_cluster_models(
            data, mn, trajectories, n_clusters=7, batch_size=64
        )

        for i in range(len(trajectories)):
            visualize_clusters_video(
                data,
                mn,
                trajectories[i],
                models_mn,
                out_dir="activation_vis/out/mobilenetv2",
            )

    if False:
        vgg = VGG11()
        models_vgg = train_cluster_models(
            data, vgg, trajectories, n_clusters=7, batch_size=16
        )

        for i in range(len(trajectories)):
            visualize_clusters_video(
                data,
                vgg,
                trajectories[i],
                models_vgg,
                out_dir="activation_vis/out/vgg",
            )

    if False:
        critic = NewCritic()
        critic.load_state_dict(
            torch.load(
                "critic/trained/saves/critic-rewidx=1-cepochs=15-datamode=trunk-datasize=100000-shift=12-chfak=1-dropout=0.3.pt"
            )
        )
        models_critic = train_cluster_models(
            data, critic, trajectories, n_clusters=7, batch_size=64
        )
        for i in range(len(trajectories)):
            visualize_clusters_video(
                data,
                critic,
                trajectories[i],
                models_critic,
                out_dir="activation_vis/out/critic",
                batch_size=128,
            )


    # for f in os.listdir(
    #     "/home/markus/uni/navigation_project/activation_vis/out/critic"
    # ):
    #     call(
    #         f"cd /home/markus/uni/navigation_project/activation_vis/out/critic && ffmpeg -i {f} -vf reverse {f.replace('.mp4', '')}_r.mp4 -crf 20",
    #         shell=True,
    #     )
