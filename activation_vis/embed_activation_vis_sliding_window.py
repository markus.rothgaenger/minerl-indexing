import itertools
import logging
from mimetypes import init
import os
from black import out

import faiss
import imageio
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import minerl
import numpy as np
import torch
import torchvision.transforms as T
from matplotlib.backends.backend_agg import FigureCanvasAgg
from minerl.data import BufferedBatchIter
from numpy import typing as npt
from torch import Tensor, nn

# eset_start_method("fork")
os.environ["MINERL_DATA_ROOT"] = f"/home/{os.environ.get('USER')}/minerl"
logging.basicConfig(level=logging.ERROR)
rng = np.random.default_rng()
data = minerl.data.make("MineRLTreechop-v0")

RGB_INDEX = -1
POOL_SIZE = 8
MAX_FRAMES = 500
EXTENT = 0, 63, 0, 63
BUFFER_SIZE = 11
DPI = 200


class MobileNetV2(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = torch.hub.load(
            "pytorch/vision:v0.11.1", "mobilenet_v2", pretrained=True
        ).eval()
        self.transforms = nn.Sequential(
            T.Resize([224, 224]),
            T.ConvertImageDtype(torch.float),
            T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
        )
        # if torch.cuda.is_available():
        #     print('cuda enabled!')
        #     self.model.to('cuda')

    def eval(self, x: Tensor) -> Tensor:
        with torch.no_grad():
            x = x.permute(0, 3, 1, 2)
            x = self.transforms(x)
            # x = x.unsqueeze(0)
            # if torch.cuda.is_available():
            #     x = x.to('cuda')
            return self.model(x)

    def eval_intermediate(self, x: Tensor) -> tuple[Tensor, list[Tensor]]:
        with torch.no_grad():
            x = x.permute(0, 3, 1, 2)
            results = []
            x = self.transforms(x)

            for layer in self.model.features:
                x = layer(x)
                results.append(x)

            x = nn.functional.adaptive_avg_pool2d(x, (1, 1))
            x = torch.flatten(x, 1)
            x = self.model.classifier(x)

            return x, results


class VGG11(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = torch.hub.load(
            "pytorch/vision:v0.11.1", "vgg11", pretrained=True
        ).eval()
        self.transforms = nn.Sequential(
            T.Resize([224, 224]),
            T.ConvertImageDtype(torch.float),
            T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]),
        )
        # if torch.cuda.is_available():
        #     print('cuda enabled!')
        #     self.model.to('cuda')

    def eval(self, x: Tensor) -> Tensor:
        with torch.no_grad():
            x = x.permute(0, 3, 1, 2)
            x = self.transforms(x)
            # x = x.unsqueeze(0)
            # if torch.cuda.is_available():
            #     x = x.to('cuda')
            return self.model(x)

    def eval_intermediate(self, x: Tensor) -> tuple[Tensor, list[Tensor]]:
        with torch.no_grad():
            x = x.permute(0, 3, 1, 2)
            results = []
            x = self.transforms(x)
            # results.append(x)
            # x = x.unsqueeze(0)
            # results.append(x)

            for layer in self.model.features:
                x = layer(x)
                results.append(x)

            x = self.model.avgpool(x)
            x = torch.flatten(x, 1)
            x = self.model.classifier(x)

            return x, results


class NewCritic(nn.Module):
    def __init__(
        self,
        width=64,
        dims=[8, 8, 8, 16],
        bottleneck=32,
        colorchs=3,
        chfak=1,
        activation=nn.ReLU,
        pool="max",
        dropout=0.5,
    ):
        super().__init__()
        self.width = width
        stride = 1 if pool == "max" else 2
        dims = np.array(dims) * chfak
        pool = nn.MaxPool2d(2) if pool == "max" else nn.Identity()
        self.pool = pool
        features = [
            nn.Conv2d(colorchs, dims[0], 3, stride, 1),
            activation(),
            pool,
            nn.Conv2d(dims[0], dims[1], 3, stride, 1),
            activation(),
            pool,
            nn.Conv2d(dims[1], dims[2], 3, stride, 1),
            activation(),
            pool,
            nn.Dropout(dropout),
            nn.Conv2d(dims[2], dims[3], 3, stride, 1),
            activation(),
            pool,
            nn.Dropout(dropout),
            nn.Conv2d(dims[3], bottleneck * chfak, 4),
            activation(),
        ]
        self.features = nn.Sequential(*features)

        self.crit = nn.Sequential(
            nn.Flatten(),
            nn.Linear(chfak * bottleneck, chfak * bottleneck),
            activation(),
            nn.Dropout(dropout),
            nn.Linear(chfak * bottleneck, 1),
            nn.Sigmoid(),
        )

    def forward(self, X, collect=False):
        embeds = []
        # print(list(self.features))
        for layer in list(self.features):
            X = layer(X)
            if collect and isinstance(layer, type(self.pool)):
                embeds.append(X)
        if collect:
            embeds.append(X)
        # print("last embed", X.shape)
        pred = self.crit(X)

        if collect:
            return pred, embeds
        else:
            return pred

    def preprocess(self, X: Tensor):
        # X = X.T.unsqueeze(0)
        return (X / 255.0).permute(0, 3, 1, 2).float()
        # return (X/255.0).float()

    def eval_intermediate(self, X):
        with torch.no_grad():
            X = self.preprocess(X)
            return self.forward(X, collect=True)


def prettySize(s: torch.Size):
    # return '-'.join([str(e) for e in s])
    return tuple([e for e in s])


def setup_axes(axes: dict[str, dict[int, any]], n_layers):
    n_vis = n_layers + 2
    n_cluster_sizes = len(CLUSTER_SIZES)
    axes["ref"] = {
        _i: plt.subplot(n_cluster_sizes + 2, n_vis, n_vis + _i + 3)
        for _i in range(n_layers)
    }
    axes["act"] = {
        _i: {
            cs: plt.subplot(n_cluster_sizes + 2, n_vis, _i + (i + 2) * n_vis + 3)
            for i, cs in enumerate(CLUSTER_SIZES)
        }
        for _i in range(n_layers)
    }
    axes["rgb_ref"] = plt.subplot(n_cluster_sizes + 2, n_vis, n_vis + 2)
    axes["rgb_act"] = {
        cs: plt.subplot(n_cluster_sizes + 2, n_vis, (i + 2) * n_vis + 2)
        for i, cs in enumerate(CLUSTER_SIZES)
    }
    axes["title"] = {
        _i: plt.subplot(n_cluster_sizes + 2, n_vis, _i + 3)
        for _i in range(-1, n_layers)
    }
    axes["cs_text"] = {
        cs: plt.subplot(n_cluster_sizes + 2, n_vis, (i + 2) * n_vis + 1)
        for i, cs in enumerate(CLUSTER_SIZES)
    }


def reset_axes(axes: dict[str, dict[int, any]]):
    for _, v in axes["ref"].items():
        v.clear()
        v.axis("off")
    for layer, _ in axes["act"].items():
        for _, v in axes["act"][layer].items():
            v.clear()
            v.axis("off")

    axes["rgb_ref"].clear()
    axes["rgb_ref"].axis("off")

    for _, v in axes["rgb_act"].items():
        v.clear()
        v.axis("off")

    for _, v in axes["cs_text"].items():
        v.clear()
        v.axis("off")

    for _, v in axes["title"].items():
        v.clear()
        v.axis("off")


def sliding_window_pov_vis(
    axes: dict[str, dict[int, any]],
    canvas: FigureCanvasAgg,
    fig,
    model: nn.Module,
    embed_models: dict[int, dict[int, faiss.Kmeans]],
    pov: Tensor,
):
    _, intermediate_act = model.eval_intermediate(Tensor(pov))

    c_values = {}

    # cluster
    for layer, _act in enumerate(intermediate_act):
        _act = _act.permute(0, 2, 3, 1)

        for cs in embed_models[layer].keys():
            kmeans = embed_models[layer][cs]

            if c_values.get(layer) is None:
                c_values[layer] = {}

            # if not trained..
            if not hasattr(kmeans, "index"):
                c_values[layer][cs] = np.full((*_act.shape[0:3], 1), -1)
                continue

            _, I = kmeans.index.search(
                np.array(_act, dtype=np.float32, order="C").reshape(
                    (-1, _act.shape[-1])
                ),
                1,
            )

            c_values[layer][cs] = I.reshape((*_act.shape[0:3], 1))

    for cs in embed_models[RGB_INDEX].keys():
        kmeans = embed_models[RGB_INDEX][cs]

        _, I = kmeans.index.search(
            np.array(pov, dtype=np.float32, order="C").reshape((-1, pov.shape[-1])),
            1,
        )

        if c_values.get(RGB_INDEX) is None:
            c_values[RGB_INDEX] = {}

        c_values[RGB_INDEX][cs] = I.reshape((*pov.shape[0:3], 1))

    need_tightness = False

    n_layers = len(intermediate_act)

    if axes["ref"] is None:
        setup_axes(axes, n_layers)
        need_tightness = True

    reset_axes(axes)

    # write
    for layer, _act in enumerate(intermediate_act):
        _act = _act[0]

        axes["ref"][layer].imshow(pov[0], extent=EXTENT)
        axes["title"][layer].text(
            0,
            0.5,
            f"{prettySize(_act.shape)}",
            size=6,
            color="white",
            fontstretch="condensed",
            in_layout=False,
        )

        for cs in CLUSTER_SIZES:
            cmap = cm.get_cmap("gist_rainbow", cs)

            # if clusters trained..
            if np.any(c_values[layer][cs][0] < 0):
                axes["act"][layer][cs].text(
                    0.2, 0.5, "no data", size=5, color="white", fontstretch="condensed"
                )
            else:
                axes["act"][layer][cs].imshow(pov[0], alpha=1.0, extent=EXTENT)
                axes["act"][layer][cs].imshow(
                    c_values[layer][cs][0],
                    cmap=cmap,
                    alpha=0.5,
                    extent=EXTENT,
                    vmin=0,
                    vmax=cs,
                )

    # RGB stuff
    axes["rgb_ref"].imshow(pov[0], extent=EXTENT)
    axes["title"][RGB_INDEX].text(
        0,
        0.5,
        f"RGB\n{prettySize(pov[0].T.shape)}",
        size=6,
        color="white",
        fontstretch="condensed",
        in_layout=False,
    )

    for cs in CLUSTER_SIZES:
        cmap = cm.get_cmap("gist_rainbow", cs)
        axes["rgb_act"][cs].imshow(pov[0], alpha=1.0, extent=EXTENT)
        axes["rgb_act"][cs].imshow(
            c_values[RGB_INDEX][cs][0],
            cmap=cmap,
            alpha=0.5,
            extent=EXTENT,
            vmin=0,
            vmax=cs,
        )

    for cs in CLUSTER_SIZES:
        axes["cs_text"][cs].text(0.5, 0.5, f"{cs}", size=6, color="white")

    if need_tightness:
        fig.patch.set_facecolor("#292929")
        height_px = 64 * (len(CLUSTER_SIZES) + 2)
        width_px = 64 * (n_layers + 2)
        fig.set_size_inches(width_px / (DPI / 2), height_px / (DPI / 2), forward=True)
        fig.tight_layout(pad=0.1)

    # fig.subplots_adjust(left=0, right=1, bottom=0, top=1)
    canvas.draw()
    s, (width, height) = canvas.print_to_buffer()

    return np.frombuffer(s, np.uint8).reshape((height, width, 4))


def sliding_window_pov_export(
    model: nn.Module,
    embed_models: dict[int, dict[int, faiss.Kmeans]],
    trajectory: str,
    out_dir: str,
    frame_idx: int,
    pov: Tensor,
):
    _, intermediate_act = model.eval_intermediate(Tensor(pov))

    c_values = {}

    # cluster
    for layer, _act in enumerate(intermediate_act):
        _act = _act.permute(0, 2, 3, 1)

        for cs in embed_models[layer].keys():
            kmeans = embed_models[layer][cs]

            if c_values.get(layer) is None:
                c_values[layer] = {}

            # if not trained..
            if not hasattr(kmeans, "index"):
                c_values[layer][cs] = np.full((*_act.shape[0:3], 1), -1)
                continue

            _, I = kmeans.index.search(
                np.array(_act, dtype=np.float32, order="C").reshape(
                    (-1, _act.shape[-1])
                ),
                1,
            )

            c_values[layer][cs] = I.reshape((*_act.shape[0:3], 1))

    # write
    for layer, _act in enumerate(intermediate_act):
        _act = _act[0]

        for cs in CLUSTER_SIZES:
            cmap = cm.get_cmap("gist_rainbow", cs)

            # if clusters trained..
            if np.any(c_values[layer][cs][0] < 0):
                pass
            else:
                path = f"{out_dir}/masks/{trajectory}/{layer}/{cs}/"
                if not os.path.exists(path):
                    os.makedirs(path)
                imageio.imwrite(f"{path}{frame_idx}.png", c_values[layer][cs][0])

    return


def sliding_window(
    model: nn.Module,
    data,
    trajectories,
    cluster_sizes: list[int],
    n_batches: int,
    out_dir: str,
):
    # stores kmeans models per layer and per cluster size
    embed_models: dict[int, dict[int, faiss.Kmeans]] = {}
    axes = {
        "act": None,
        "cs_text": None,
        "ref": None,
        "rgb_act": None,
        "rgb_ref": None,
        "title": None,
    }
    fig = plt.figure(figsize=(10, 1), dpi=DPI)
    canvas = FigureCanvasAgg(fig)
    # median observation as pov
    pov_idx = int(np.floor(BUFFER_SIZE / 2))

    for _, trajectory in enumerate(trajectories):
        iterator = BufferedBatchIter(data)
        iterator.available_trajectories = [trajectory]

        pov_buffer = []
        embed_buffer = {}

        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        n_frames = 0

        with imageio.get_writer(
            f"{out_dir}/{trajectory}.mp4", mode="I", fps=25
        ) as writer:
            for state, _, _, _, _ in iterator.buffered_batch_iter(
                batch_size=1, num_batches=n_batches
            ):
                _, embed_act = model.eval_intermediate(Tensor(state["pov"]))

                _pov = state["pov"]
                pov_buffer.append(_pov)
                if len(pov_buffer) > BUFFER_SIZE:
                    pov_buffer.pop(0)

                for i in range(len(embed_act)):
                    if embed_buffer.get(i) is None:
                        embed_buffer[i] = []

                    _act = embed_act[i].permute(0, 2, 3, 1)
                    _shape = _act.shape
                    _data = (
                        _act.numpy()
                        .reshape((-1, _shape[3]))
                        .astype(np.float32, order="C")
                    )
                    _, n_channels = _data.shape

                    embed_buffer[i].append(_data)
                    if len(embed_buffer[i]) > BUFFER_SIZE:
                        embed_buffer[i].pop(0)

                    if embed_models.get(i) is None:
                        embed_models[i] = {}

                        for cs in cluster_sizes:
                            embed_models[i][cs] = faiss.Kmeans(n_channels, cs)

                    if len(pov_buffer) > pov_idx:
                        for cs in cluster_sizes:
                            _data = np.concatenate(
                                [x for x in embed_buffer[i]], axis=0, dtype=np.float32
                            )
                            if embed_models[i][cs].k > len(_data):
                                continue

                            init_centroids = embed_models[i][cs].centroids
                            embed_models[i][cs].train(
                                _data, init_centroids=init_centroids
                            )

                # RGB STUFF
                _shape = _pov.shape
                _data = _pov.reshape((-1, _shape[3])).astype(np.float32, order="C")
                _, n_channels = _data.shape
                if embed_buffer.get(RGB_INDEX) is None:
                    embed_buffer[RGB_INDEX] = []
                embed_buffer[RGB_INDEX].append(_data)
                if len(embed_buffer[RGB_INDEX]) > BUFFER_SIZE:
                    embed_buffer[RGB_INDEX].pop(0)

                if embed_models.get(RGB_INDEX) is None:
                    embed_models[RGB_INDEX] = {}

                    for cs in cluster_sizes:
                        embed_models[RGB_INDEX][cs] = faiss.Kmeans(n_channels, cs)

                for cs in cluster_sizes:
                    init_centroids = embed_models[RGB_INDEX][cs].centroids
                    if len(pov_buffer) > pov_idx:
                        _data = np.concatenate(
                            [x for x in embed_buffer[RGB_INDEX]],
                            axis=0,
                            dtype=np.float32,
                        )
                        embed_models[RGB_INDEX][cs].train(
                            _data, init_centroids=init_centroids
                        )

                if len(pov_buffer) > pov_idx:
                    writer.append_data(
                        sliding_window_pov_vis(
                            axes=axes,
                            canvas=canvas,
                            fig=fig,
                            model=model,
                            embed_models=embed_models,
                            pov=pov_buffer[pov_idx],
                        )
                    )
                    n_frames += 1
                    print(f"frame: {n_frames}")


def sliding_window_export(
    model: nn.Module,
    data,
    trajectories,
    cluster_sizes: list[int],
    n_batches: int,
    out_dir: str,
):
    # stores kmeans models per layer and per cluster size
    embed_models: dict[int, dict[int, faiss.Kmeans]] = {}
    # median observation as pov
    pov_idx = int(np.floor(BUFFER_SIZE / 2))

    for _, trajectory in enumerate(trajectories):
        iterator = BufferedBatchIter(data)
        iterator.available_trajectories = [trajectory]

        pov_buffer = []
        embed_buffer = {}

        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        n_frames = 0

        for state, _, _, _, _ in iterator.buffered_batch_iter(
            batch_size=1, num_batches=n_batches
        ):
            _, embed_act = model.eval_intermediate(Tensor(state["pov"]))

            _pov = state["pov"]
            pov_buffer.append(_pov)
            if len(pov_buffer) > BUFFER_SIZE:
                pov_buffer.pop(0)

            for i in range(len(embed_act)):
                if embed_buffer.get(i) is None:
                    embed_buffer[i] = []

                _act = embed_act[i].permute(0, 2, 3, 1)
                _shape = _act.shape
                _data = (
                    _act.numpy().reshape((-1, _shape[3])).astype(np.float32, order="C")
                )
                _, n_channels = _data.shape

                embed_buffer[i].append(_data)
                if len(embed_buffer[i]) > BUFFER_SIZE:
                    embed_buffer[i].pop(0)

                if embed_models.get(i) is None:
                    embed_models[i] = {}

                    for cs in cluster_sizes:
                        embed_models[i][cs] = faiss.Kmeans(n_channels, cs)

                if len(pov_buffer) > pov_idx:
                    for cs in cluster_sizes:
                        _data = np.concatenate(
                            [x for x in embed_buffer[i]], axis=0, dtype=np.float32
                        )
                        if embed_models[i][cs].k > len(_data):
                            continue

                        init_centroids = embed_models[i][cs].centroids
                        embed_models[i][cs].train(_data, init_centroids=init_centroids)

            if len(pov_buffer) > pov_idx:
                sliding_window_pov_export(
                    model=model,
                    embed_models=embed_models,
                    trajectory=trajectory,
                    out_dir=out_dir,
                    frame_idx=n_frames,
                    pov=pov_buffer[pov_idx],
                )
                n_frames += 1
                print(f"frame: {n_frames}")


if __name__ == "__main__":
    trajectories = [
        "v3_svelte_cherry_devil-12_30091-31945",
        "v3_subtle_iceberg_lettuce_nymph-6_203-2056",
        "v3_absolute_grape_changeling-16_2277-4441",
        "v3_content_squash_angel-3_16074-17640",
        "v3_smooth_kale_loch_ness_monster-1_4439-6272",
        "v3_cute_breadfruit_spirit-6_17090-19102",
        "v3_key_nectarine_spirit-2_7081-9747",
        "v3_subtle_iceberg_lettuce_nymph-6_3819-6049",
        "v3_juvenile_apple_angel-30_396415-398113",
        "v3_subtle_iceberg_lettuce_nymph-6_6100-8068",
    ]

    # TODO: generalize cluster size when creating video.
    CLUSTER_SIZES = [4, 8, 16]

    if False:
        mobilenet = MobileNetV2()
        sliding_window(
            model=mobilenet,
            data=data,
            trajectories=trajectories,
            cluster_sizes=CLUSTER_SIZES,
            n_batches=500,
            out_dir="activation_vis/out/mobilenet",
        )

    if False:
        vgg = VGG11()
        sliding_window_export(
            model=vgg,
            data=data,
            trajectories=trajectories,
            cluster_sizes=CLUSTER_SIZES,
            n_batches=1000,
            out_dir="activation_vis/out/vgg",
        )

    if True:
        critic = NewCritic()
        critic.load_state_dict(
            torch.load(
                "critic/trained/saves/critic-rewidx=1-cepochs=15-datamode=trunk-datasize=100000-shift=12-chfak=1-dropout=0.3.pt"
            )
        )
        sliding_window_export(
            model=critic,
            data=data,
            trajectories=trajectories[2:],
            cluster_sizes=CLUSTER_SIZES,
            n_batches=1000,
            out_dir="activation_vis/out/critic",
        )
