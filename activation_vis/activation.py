import gc
import logging
import os
from multiprocessing import Pool, set_start_method
from subprocess import call

import faiss
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import minerl
import numpy as np
import torch
import torchvision.transforms as T
from matplotlib.backends.backend_agg import FigureCanvasAgg
from minerl.data import BufferedBatchIter
from numpy import dtype, typing as npt
from torch import Tensor, nn
from typing import Union

set_start_method('fork')
os.environ['MINERL_DATA_ROOT'] = '/home/markus/minerl'
logging.basicConfig(level=logging.ERROR)
rng = np.random.default_rng()


class MobileNetV2(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = torch.hub.load(
            'pytorch/vision:v0.11.1', 'mobilenet_v2', pretrained=True).eval()
        self.transforms = nn.Sequential(
            T.Resize([224, 224]),
            T.ConvertImageDtype(torch.float),
            T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        )
        # if torch.cuda.is_available():
        #     print('cuda enabled!')
        #     self.model.to('cuda')

    def eval(self, x: Tensor) -> Tensor:
        with torch.no_grad():
            x = self.transforms(x)
            x = x.unsqueeze(0)
            # if torch.cuda.is_available():
            #     x = x.to('cuda')
            return self.model(x)

    def eval_intermediate(self, x: Tensor) -> tuple[Tensor, list[Tensor]]:
        with torch.no_grad():
            results = []
            x = self.transforms(x)
            # results.append(x)
            x = x.unsqueeze(0)
            # results.append(x)

            for layer in self.model.features:
                x = layer(x)
                results.append(x)

            x = nn.functional.adaptive_avg_pool2d(x, (1, 1))
            # results.append(x)
            x = torch.flatten(x, 1)
            # results.append(x)
            x = self.model.classifier(x)
            # results.append(x)

            return x, results


class VGG11(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = torch.hub.load(
            'pytorch/vision:v0.11.1', 'vgg11', pretrained=True).eval()
        self.transforms = nn.Sequential(
            T.Resize([224, 224]),
            T.ConvertImageDtype(torch.float),
            T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        )
        # if torch.cuda.is_available():
        #     print('cuda enabled!')
        #     self.model.to('cuda')

    def eval(self, x: Tensor) -> Tensor:
        with torch.no_grad():
            x = self.transforms(x)
            x = x.unsqueeze(0)
            # if torch.cuda.is_available():
            #     x = x.to('cuda')
            return self.model(x)

    def eval_intermediate(self, x: Tensor) -> tuple[Tensor, list[Tensor]]:
        with torch.no_grad():
            results = []
            x = self.transforms(x)
            # results.append(x)
            x = x.unsqueeze(0)
            # results.append(x)

            for layer in self.model.features:
                x = layer(x)
                results.append(x)

            x = self.model.avgpool(x)
            x = torch.flatten(x, 1)
            x = self.model.classifier(x)

            return x, results


def prettySize(s: torch.Size):
    # return '-'.join([str(e) for e in s])
    return tuple([e for e in s])


STATE_PER_TRAJECTORY = 100


def train_cluster_models(model, iterator, trajectories: list[str], n_clusters) -> tuple[list[list[dict]], dict[int, tuple[faiss.Kmeans, faiss.Kmeans]]]:
    layer_outputs: dict[int, list[Union[Tensor, npt.NDArray]]] = None
    kmeans_models: dict[int, tuple[faiss.Kmeans, faiss.Kmeans]] = {}
    test_data: npt.NDArray = np.empty(
        (len(trajectories), ), dtype=object)

    n_trajectory = 0
    for trajectory in trajectories:
        state_count = 0
        trajectory_test_data = np.empty(((5,)), dtype=object)
        n_test = 0

        for current_state, _, _, _, _ in iterator.data_pipeline.load_data(trajectory):
            if np.mod(state_count, 20) == 0:
                trajectory_test_data[n_test] = current_state['pov']
                n_test += 1

            _, intermediate_act = model.eval_intermediate(
                Tensor(current_state['pov'].T))

            if layer_outputs is None:
                layer_outputs = {}

                for i in range(len(intermediate_act)):
                    layer_outputs[i] = [intermediate_act[i].numpy()]
                layer_outputs[-1] = [current_state['pov']]
            else:
                for i in range(len(intermediate_act)):
                    layer_outputs[i].append(intermediate_act[i].numpy())
                layer_outputs[-1].append(current_state['pov'])

            state_count += 1

            if state_count > 99:
                break

        test_data[n_trajectory] = trajectory_test_data
        n_trajectory += 1

    for k, v in layer_outputs.items():
        layer_activations = np.array([np.array(v, dtype=np.float32)]) if k < 0 else np.array(
            [x[0].T for x in v])

        _shape = layer_activations.shape
        data = layer_activations.reshape(-1, *_shape[-1:])

        _, n_channels = data.shape

        kmeans = faiss.Kmeans(n_channels, n_clusters)
        kmeansX2 = faiss.Kmeans(n_channels, n_clusters * 2 + 1)
        kmeans.train(np.ascontiguousarray(data))
        kmeansX2.train(np.ascontiguousarray(data))

        del layer_activations

        kmeans_models[k] = (kmeans, kmeansX2)

    return test_data, kmeans_models


def visualize_clusters(model, test_data: list[npt.NDArray], kmeans_models: dict[int, tuple[faiss.Kmeans, faiss.Kmeans]]):
    extent = 0, 63, 0, 63
    state_count = 0

    for current_state in test_data:
        _, intermediate_act = model.eval_intermediate(
            Tensor(current_state.T))

        fig = plt.figure(figsize=(13, 2), dpi=200)

        vis_len = len(intermediate_act) + 1
        layer = 0

        for _act in intermediate_act:
            ref_ax = plt.subplot(3, vis_len, layer+2)
            act_ax = plt.subplot(3, vis_len, layer+vis_len+2)
            actX2_ax = plt.subplot(3, vis_len, layer+2*vis_len+2)

            _act = _act[0] if _act.shape[0] == 1 else _act

            kmeans, kmeansX2 = kmeans_models[layer]

            values = np.zeros((_act.shape[-2], _act.shape[-1]), dtype=int)
            for i in range(_act.shape[-2]):
                _, I = kmeans.index.search(
                    np.ascontiguousarray(_act[:, i, :].T.numpy()), 1)
                values[i, :] = np.array(I.T)
            valuesX2 = np.zeros((_act.shape[-2], _act.shape[-1]), dtype=int)
            for i in range(_act.shape[-2]):
                _, I = kmeansX2.index.search(
                    np.ascontiguousarray(_act[:, i, :].T.numpy()), 1)
                valuesX2[i, :] = np.array(I.T)

            cmap = cm.get_cmap('gist_rainbow', kmeans.k)
            cmapX2 = cm.get_cmap('gist_rainbow', kmeansX2.k)
            ref_ax.imshow(current_state, extent=extent)
            act_ax.imshow(current_state, alpha=1.0, extent=extent)
            # transpose as pov is transposed when creating tensor!
            act_ax.imshow(values.T, cmap=cmap, alpha=0.5, extent=extent)
            act_ax.axis('off')
            actX2_ax.imshow(current_state, alpha=1.0, extent=extent)
            # transpose as pov is transposed when creating tensor!
            actX2_ax.imshow(valuesX2.T, cmap=cmapX2, alpha=0.5, extent=extent)
            actX2_ax.axis('off')
            ref_ax.axis('off')
            ref_ax.set_title(
                f"n_c: {kmeans.k}/{kmeansX2.k}\n {prettySize(_act.shape)}", fontdict={'fontsize': 6})

            layer += 1

        ref_ax = plt.subplot(3, vis_len, 1)
        act_ax = plt.subplot(3, vis_len, vis_len+1)
        actX2_ax = plt.subplot(3, vis_len, 2*vis_len+1)

        _act = np.array(current_state, dtype=np.float32)

        kmeans, kmeansX2 = kmeans_models[-1]

        values = np.zeros((_act.shape[0], _act.shape[1]), dtype=int)
        for i in range(_act.shape[0]):
            _, I = kmeans.index.search(
                np.ascontiguousarray(_act[:, i, :]), 1)
            values[i, :] = np.array(I.T)
        valuesX2 = np.zeros((_act.shape[0], _act.shape[1]), dtype=int)
        for i in range(_act.shape[0]):
            _, I = kmeans.index.search(
                np.ascontiguousarray(_act[:, i, :]), 1)
            valuesX2[i, :] = np.array(I.T)

        cmap = cm.get_cmap('gist_rainbow', kmeans.k)
        cmapX2 = cm.get_cmap('gist_rainbow', kmeansX2.k)
        ref_ax.imshow(current_state, extent=extent)
        act_ax.imshow(current_state, alpha=1.0, extent=extent)
        # transpose as pov is transposed when creating tensor!
        act_ax.imshow(values.T, cmap=cmap, alpha=0.5, extent=extent)
        act_ax.axis('off')
        actX2_ax.imshow(current_state, alpha=1.0, extent=extent)
        # transpose as pov is transposed when creating tensor!
        actX2_ax.imshow(valuesX2.T, cmap=cmapX2, alpha=0.5, extent=extent)
        actX2_ax.axis('off')
        ref_ax.axis('off')
        ref_ax.set_title(
            f"RGB n_c: {kmeans.k}/{kmeansX2.k}\n {prettySize(_act.shape)}", fontdict={'fontsize': 6})

        try:
            os.mkdir(f'out/')
        except:  # ignore already exists..
            None

        fig.tight_layout(pad=0.1)
        fig.savefig(f'out/{state_count}.png', facecolor='white')
        plt.close(fig)
        plt.clf()

        state_count += 1

    plt.close()


def dump_garbage():
    """
    show us what's the garbage about
    """

    # force collection
    print("\nGARBAGE:")
    gc.collect()

    print("\nGARBAGE OBJECTS:")
    for x in gc.garbage:
        s = str(x)
        if len(s) > 80:
            s = s[:80]
        print(type(x), "\n  ", s)


if __name__ == "__main__":
    import gc
    gc.enable()
    # gc.set_debug(gc.DEBUG_LEAK)
    mn = MobileNetV2()
    vgg = VGG11()

    data = minerl.data.make('MineRLTreechop-v0')
    iterator = BufferedBatchIter(data)
    # trajectories = iterator.available_trajectories[0:10]
    trajectories = ['v3_svelte_cherry_devil-12_30091-31945', 'v3_subtle_iceberg_lettuce_nymph-6_203-2056', 'v3_absolute_grape_changeling-16_2277-4441', 'v3_content_squash_angel-3_16074-17640', 'v3_smooth_kale_loch_ness_monster-1_4439-6272',
                    'v3_cute_breadfruit_spirit-6_17090-19102', 'v3_key_nectarine_spirit-2_7081-9747', 'v3_subtle_iceberg_lettuce_nymph-6_3819-6049', 'v3_juvenile_apple_angel-30_396415-398113', 'v3_subtle_iceberg_lettuce_nymph-6_6100-8068']
    # print(trajectories)

    # test_data_mn, models_mn = train_cluster_models(
    #     mn, iterator, trajectories, n_clusters=10)

    # print('well')

    # for i in range(len(test_data_mn)):
    #     visualize_clusters(mn, test_data_mn[i], models_mn)
    #     call(
    #         f"cd /home/markus/uni/navigation_project/activation_vis/out && convert *.png -append mobilenetv2/layer_res{i:02}.png", shell=True)

    # del test_data_mn
    # del models_mn
    # del mn
    # del iterator
    # del data

    # # show the dirt ;-)
    # dump_garbage()

    # data = minerl.data.make('MineRLTreechop-v0')
    # iterator = BufferedBatchIter(data)

    test_data_vgg, models_vgg = train_cluster_models(
        vgg, iterator, trajectories, n_clusters=7)
    for i in range(len(test_data_vgg)):
        visualize_clusters(vgg, test_data_vgg[i], models_vgg)
        call(
            f"cd /home/markus/uni/navigation_project/activation_vis/out && convert *.png -append vgg/layer_res{i:02}.png", shell=True)
