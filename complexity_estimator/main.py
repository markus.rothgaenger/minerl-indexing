import argparse
import os
import sys

import matplotlib
from matplotlib.pyplot import plot
import numpy as np
import torch
from matplotlib import cm
from torch.utils.data import DataLoader, RandomSampler, Subset
from torchvision.transforms import transforms

from complexity import (
    compression_measure,
    fft_measure,
    multidim_complexity,
    pixelwise_complexity_measure,
)
from data import get_dino_transforms, get_weighted_sampler, load_mpeg7_data
from models import load_models
from plot import (
    create_vis,
    visualize_reconstructions,
    visualize_sort,
    visualize_sort_mean_std,
    visualize_sort_multidim,
)
from utils import find_components

LOAD_PRETRAINED = True
LR = 0.75e-3
EPOCHS = 50

sys.setrecursionlimit(1000000)
n_clusters = 5


def multi_dim(max_norm=True, sort=False):
    img_transformer = get_dino_transforms()
    image_generator = create_vis(n_imgs=25, sort=sort)
    color_map = cm.get_cmap("plasma")
    model_bn8, model_bn32 = load_models()

    for img in image_generator:
        np_img = np.array(img)
        rgb_np_img = np.dstack((np_img, np_img, np_img))
        cluster_img = (np_img / (255 / (n_clusters - 1))).astype(np.int8)

        start_label = 1
        complexiy_vectors = []
        all_labels = np.zeros_like(np_img, dtype=np.uint8)

        for i in range(n_clusters):
            cluster_image = (cluster_img == i).astype(np.int8)

            labels = find_components(cluster_image, start_label)

            for l in range(start_label, labels.max() + 1):
                mask = (labels == l).astype(np.float32)
                if not mask.sum() > 0:
                    continue

                normalized_img = img_transformer(transforms.F.to_pil_image(mask))

                complexity_vector = multidim_complexity(
                    normalized_img,
                    [
                        ("compression", compression_measure, [True]),
                        ("fft", fft_measure, []),
                        (
                            "pixelwise",
                            pixelwise_complexity_measure,
                            [model_bn32, model_bn8, True],
                        ),
                    ],
                )

                complexiy_vectors.append(complexity_vector)

            start_label = labels.max() + 1 if labels.max() > 0 else start_label
            all_labels += labels

        complexity_vectors = torch.stack(complexiy_vectors, dim=0)

        if max_norm:
            maxs = complexity_vectors.max(dim=0)
            complexity_vectors /= maxs.values

        complexity_norm = torch.linalg.vector_norm(complexity_vectors, dim=1)

        # rank sorted complexities descending
        color_idx = torch.argsort(torch.argsort(-complexity_norm))

        color_values = (color_idx / len(color_idx)).numpy()

        rgb_np_img[all_labels == 0, :] = np.array([0, 0, 0])

        for l in range(1, all_labels.max() + 1):
            r, g, b = matplotlib.colors.to_rgb(color_map(color_values[l - 1]))
            rgb_np_img[all_labels == l, :] = np.array([r, g, b]) * 255.0

        if sort:
            image_generator.send(
                (transforms.F.to_pil_image(rgb_np_img), complexity_norm.sum())
            )
        else:
            image_generator.send(transforms.F.to_pil_image(rgb_np_img))


def single_dim(sort=False):
    img_transformer = get_dino_transforms()
    image_generator = create_vis(n_imgs=25, sort=sort)
    color_map = cm.get_cmap("plasma")
    model_bn8, model_bn32 = load_models()

    for img in image_generator:
        np_img = np.array(img)
        rgb_np_img = np.dstack((np_img, np_img, np_img))
        cluster_img = (np_img / (255 / (n_clusters - 1))).astype(np.int8)

        start_label = 1
        complexities = []
        all_labels = np.zeros_like(np_img, dtype=np.uint8)

        for i in range(n_clusters):
            cluster_image = (cluster_img == i).astype(np.int8)

            labels = find_components(cluster_image, start_label)

            for l in range(start_label, labels.max() + 1):
                mask = (labels == l).astype(np.float32)
                if not mask.sum() > 0:
                    continue

                normalized_img = img_transformer(transforms.F.to_pil_image(mask))

                # complexity, _ = compression_measure(normalized_img, True)
                complexity, _ = pixelwise_complexity_measure(
                    normalized_img, model_bn32, model_bn8, fill_ratio_norm=True
                )
                # complexity, _ = fft_measure(normalized_img)
                complexities.append(complexity)

            start_label = labels.max() + 1 if labels.max() > 0 else start_label
            all_labels += labels

        # rank sorted complexities descending
        color_idx = np.argsort(np.argsort(-np.array(complexities)))

        color_values = color_idx / len(color_idx)

        rgb_np_img[all_labels == 0, :] = np.array([0, 0, 0])

        for l in range(1, all_labels.max() + 1):
            r, g, b = matplotlib.colors.to_rgb(color_map(color_values[l - 1]))
            rgb_np_img[all_labels == l, :] = np.array([r, g, b]) * 255.0

        # TODO: divide sum by number of labels
        # 3 - 4 pages paper
        # well described method & results
        # draft paper: figures & chapters
        if sort:
            image_generator.send(
                (transforms.F.to_pil_image(rgb_np_img), np.sum(complexities))
            )
        else:
            image_generator.send(transforms.F.to_pil_image(rgb_np_img))


def get_argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--train", action="store_true")
    parser.add_argument("--epochs", type=int, default=50)

    return parser


def parse_args(parser: argparse.ArgumentParser):
    global LOAD_PRETRAINED, EPOCHS
    args = parser.parse_args()
    LOAD_PRETRAINED = not args.train
    EPOCHS = args.epochs

    return args


# TODO: merge two bottlenecks as one model?! train with both losses..
# TODO: make test/train assignment static to preserve test/train split over all runs

if __name__ == "__main__":
    parse_args(get_argparser())
    model_bn16, model_bn64 = load_models(LOAD_PRETRAINED)
    dataset = load_mpeg7_data()

    train_size = int(0.8 * len(dataset))
    test_size = len(dataset) - train_size
    train_dataset, test_dataset = torch.utils.data.random_split(
        dataset, [train_size, test_size]
    )
    train_loader = DataLoader(train_dataset, batch_size=256, shuffle=True)
    test_loader = DataLoader(test_dataset, batch_size=256, shuffle=True)

    if not LOAD_PRETRAINED:
        if not os.path.exists("trained"):
            os.makedirs("trained")
        min_loss64 = np.inf
        min_loss16 = np.inf
        for epoch in range(EPOCHS):
            avg_loss_bn64 = model_bn64.train_loop(epoch, train_loader)
            avg_loss_bn16 = model_bn16.train_loop(epoch, train_loader)

            if avg_loss_bn64 < min_loss64:
                torch.save(
                    model_bn64.state_dict(),
                    f"trained/CONVVAE{model_bn64.bottleneck}.pth",
                )

            if avg_loss_bn16 < min_loss16:
                torch.save(
                    model_bn16.state_dict(),
                    f"trained/CONVVAE{model_bn16.bottleneck}.pth",
                )

    model_bn64.eval()
    model_bn16.eval()

    # sampler = get_weighted_sampler(
    #     dataset,
    #     [
    #         "apple",
    #         "bone",
    #         "butterfly",
    #         "hammer",
    #         "pocket",
    #         "device0",
    #         "crown",
    #         "hammer",
    #         "tree",
    #         "rat",
    #     ],
    # )

    indices = torch.randperm(len(dataset))[:15]
    subset = Subset(dataset, indices)
    sampler = RandomSampler(dataset, replacement=True, num_samples=100)
    data_loader = DataLoader(dataset, batch_size=1, sampler=sampler)
    subset_loader = DataLoader(subset, batch_size=1)

    # visualize_reconstructions(
    #     data_loader, model_bn64, torch.argmax, plot_histogram=True
    # )
    # visualize_reconstructions(
    #     data_loader, model_bn64, torch.argmin, plot_histogram=True
    # )

    # visualize_sort_mean_std(
    #     data_loader,
    #     pixelwise_complexity_measure,
    #     "px_recon_complexity",
    #     model_bn64,
    #     model_bn16,
    #     fill_ratio_norm=False,
    #     return_mean_std=True,
    # )
    visualize_sort(
        subset_loader,
        pixelwise_complexity_measure,
        "px_recon_complexity",
        model_bn64,
        model_bn16,
        rows=1,
        cols=len(indices),
        fill_ratio_norm=False,
    )
    visualize_sort(
        subset_loader,
        compression_measure,
        "compression",
        rows=1,
        cols=len(indices),
        fill_ratio_norm=True,
    )
    visualize_sort(subset_loader, fft_measure, "fft", rows=1, cols=len(indices))
    visualize_sort_multidim(
        subset_loader,
        [
            ("fft", fft_measure, []),
            ("compression", compression_measure, [True]),
            (
                "px_recon_comp",
                pixelwise_complexity_measure,
                [model_bn64, model_bn16, False],
            ),
        ],
        max_norm=False,
        rows=1,
        cols=len(indices),
    )
    visualize_sort_multidim(
        subset_loader,
        [
            ("fft", fft_measure, []),
            ("compression", compression_measure, [True]),
            (
                "px_recon_comp",
                pixelwise_complexity_measure,
                [model_bn64, model_bn16, False],
            ),
        ],
        max_norm=True,
        rows=1,
        cols=len(indices),
    )

    # visualize_sort_multidim(
    #     data_loader,
    #     [
    #         ("fft", fft_measure, []),
    #         ("compression", compression_measure, [False]),
    #         (
    #             "px_recon_comp",
    #             pixelwise_complexity_measure,
    #             [model_bn64, model_bn16, False],
    #         ),
    #     ],
    #     max_norm=False,
    # )
