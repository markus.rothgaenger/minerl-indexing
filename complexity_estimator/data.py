import os
from typing import Callable

import numpy as np
import torch
from kornia.morphology import closing
from PIL import Image
from torch import Tensor
from torch.utils.data import Dataset, Subset, WeightedRandomSampler
from torchvision.transforms import transforms

from utils import bbox


class MPEG7ShapeDataset(Dataset):
    img_dir: str
    filenames: list[str] = []
    labels: list[str] = []
    label_dict: dict[str]
    transform: Callable = None

    def __init__(self, img_dir, transform=None):
        self.img_dir = img_dir
        self.transform = transform

        paths = os.listdir(self.img_dir)
        labels = []
        for file in paths:
            fp = os.path.join(self.img_dir, file)
            if os.path.isfile(fp):
                label = file.split("-")[0].lower()
                self.filenames.append(fp)
                labels.append(label)

        label_name_dict = dict.fromkeys(labels)

        self.label_dict = {i: v for (i, v) in enumerate(label_name_dict.keys())}
        self.label_index_dict = {v: i for (i, v) in self.label_dict.items()}
        self.labels = [self.label_index_dict[l] for l in labels]

    def __len__(self):
        return len(self.filenames)

    def __getitem__(self, idx):
        img_path = self.filenames[idx]
        gif = Image.open(img_path)
        gif.convert("RGB")
        label = self.labels[idx]
        if self.transform:
            image = self.transform(gif)
        return image, label


class BBoxTransform:
    squared: bool = False

    def __init__(self, squared: bool = None) -> None:
        if squared is not None:
            self.squared = squared

    def __call__(self, img: any) -> Tensor:
        img = transforms.F.to_tensor(img).squeeze(dim=0)
        ymin, ymax, xmin, xmax = bbox(img)
        if not self.squared:
            return transforms.F.to_pil_image(img[ymin:ymax, xmin:xmax].unsqueeze(dim=0))

        max_dim = (ymin, ymax) if ymax - ymin > xmax - xmin else (xmin, xmax)
        n = max_dim[1] - max_dim[0]
        if n % 2 != 0:
            n += 1

        n_med = np.round(n / 2)

        ymedian = np.round(ymin + (ymax - ymin) / 2)
        xmedian = np.round(xmin + (xmax - xmin) / 2)

        M, N = img.shape

        ycutmin, ycutmax = (
            int(ymedian - n_med if ymedian >= n_med else 0),
            int(ymedian + n_med if ymedian + n_med <= M else M),
        )

        xcutmin, xcutmax = (
            int(xmedian - n_med if xmedian >= n_med else 0),
            int(
                xmedian + n_med if xmedian + n_med <= N else N,
            ),
        )

        if (ycutmax - ycutmin) % 2 != 0:
            ycutmin += 1
        if (xcutmax - xcutmin) % 2 != 0:
            xcutmin += 1

        squared_x = np.zeros((n, n))
        squared_cut_y = np.round((ycutmax - ycutmin) / 2)
        squared_cut_x = np.round((xcutmax - xcutmin) / 2)

        dest_ymin, dest_ymax = int(n_med - squared_cut_y), int(n_med + squared_cut_y)
        dest_xmin, dest_xmax = int(n_med - squared_cut_x), int(n_med + squared_cut_x)

        squared_x[
            dest_ymin:dest_ymax,
            dest_xmin:dest_xmax,
        ] = img[ycutmin:ycutmax, xcutmin:xcutmax]

        return transforms.F.to_pil_image(torch.from_numpy(squared_x).unsqueeze(dim=0))


class CloseTransform:
    kernel = torch.ones(5, 5)

    def __init__(self, kernel=None):
        if kernel is not None:
            self.kernel = kernel

    def __call__(self, x):
        x = transforms.F.to_tensor(x)

        if len(x.shape) < 4:
            return transforms.F.to_pil_image(
                closing(x.unsqueeze(dim=0), self.kernel).squeeze(dim=0)
            )

        return transforms.F.to_pil_image(closing(x, self.kernel))


def get_dino_transforms():
    return transforms.Compose(
        [
            transforms.Grayscale(),
            # transforms.RandomApply([CloseTransform()], p=0.25),
            BBoxTransform(squared=True),
            transforms.Resize(
                (64, 64), interpolation=transforms.InterpolationMode.NEAREST
            ),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.ToTensor(),
        ]
    )


def load_mpeg7_data():
    transform = transforms.Compose(
        [
            transforms.Grayscale(),
            # transforms.RandomApply([CloseTransform()], p=0.25),
            transforms.RandomRotation(degrees=85, expand=True),
            BBoxTransform(squared=True),
            transforms.Resize(
                (64, 64), interpolation=transforms.InterpolationMode.NEAREST
            ),
            transforms.RandomHorizontalFlip(),
            transforms.RandomVerticalFlip(),
            transforms.ToTensor(),
        ]
    )

    return MPEG7ShapeDataset("../shape_complexity/data/mpeg7", transform)


def get_weighted_sampler(dataset: MPEG7ShapeDataset, label_names: list):
    label_indices = [dataset.label_index_dict[name] for name in label_names]
    # indices = [
    #     idx for idx, label in enumerate(dataset.labels) if label in label_indices
    # ]
    # return Subset(dataset, indices)

    label_weights = [
        1 if label in label_indices else 0 for _, label in enumerate(dataset.labels)
    ]

    return WeightedRandomSampler(weights=label_weights, num_samples=100)
