import glob
import io
import os
from typing import Callable, Generator

import numpy as np
import numpy.typing as npt
import torch
from matplotlib import pyplot as plt
from PIL import Image as img
from PIL.Image import Image
from torch import Tensor, std_mean
from torch.utils.data import DataLoader
from torchvision.utils import save_image
import torchvision.transforms.functional as F

from utils import natsort
from complexity import pixelwise_complexity_measure
from models import CONVVAE


def create_vis(
    n_imgs, layer_range=range(12), sort=False
) -> Generator[Image, Image, None]:
    """
    yields three channel PIL image

    original by @wlad
    """
    inp = "./data/dino/"
    out = "./results/comp_fft_px_sorted/"
    os.makedirs(out, exist_ok=True)
    rltrenner = img.open("./data/static/rltrenner.png")
    rltrenner = rltrenner.convert("RGB")

    yseperator = 20
    print("Creating images")
    for i in range(n_imgs):  # TODO: maybe add tqdm again..
        imagesvert = []
        name = "img" + str(i)

        aimg = img.open(inp + name + ".png")
        aimg = aimg.convert("RGB")

        for depth in layer_range:
            imagesinline = [aimg]

            for attention_type in ["q", "k", "v"]:
                complexities = []
                _attention_images = []

                img_paths = sorted(
                    glob.glob(
                        os.path.join(
                            inp, f"{name}{attention_type}depth{str(depth)}head*.png"
                        )
                    ),
                    key=natsort,
                )

                for path in img_paths:
                    image = img.open(path)

                    if sort:
                        image, complexity = yield image
                    else:
                        image = yield image

                    yield

                    # image = image.convert("RGB")

                    image = image.resize((480, 480), resample=img.NEAREST)

                    _attention_images.append(image)
                    if sort:
                        complexities.append(complexity)

                if sort:
                    sort_idx = np.argsort(complexities)
                    _attention_images = [_attention_images[i] for i in sort_idx]

                _attention_images.insert(0, rltrenner)
                imagesinline.extend(_attention_images)

            widths, heights = zip(*(i.size for i in imagesinline))

            total_width = sum(widths)
            max_height = max(heights)

            vertical_img = img.new("RGB", (total_width, max_height))

            x_offset = 0
            for im in imagesinline:
                vertical_img.paste(im, (x_offset, 0))
                x_offset += im.size[0]
            imagesvert.append(vertical_img.convert("RGB"))

        widths, heights = zip(*(i.size for i in imagesvert))

        total_height = sum(heights) + (len(layer_range) * yseperator)
        max_width = max(widths)

        final_img = img.new("RGB", (max_width, total_height))

        y_offset = 0
        for im in imagesvert:
            final_img.paste(im, (0, y_offset))
            y_offset += im.size[1] + yseperator

        final_img.save(os.path.join(out, "img" + str(i) + ".png"))


# TODO: instead of plotting each mask individually, create big image array/tensor
def plot_samples(
    masks: Tensor, ratings: npt.NDArray, rows=10, cols=10, labels: npt.NDArray = None
):
    dpi = 150
    total = rows * cols
    n_samples, _, y, x = masks.shape

    extent = (0, x - 1, 0, y - 1)
    if labels is not None:
        label_map = {
            v: i + 1 for i, v in enumerate({int(v): int(v) for v in labels}.keys())
        }
        max_label = len(label_map) + 1

    if total != n_samples:
        raise Exception("shape mismatch")

    fig = plt.figure(figsize=(32, 16), dpi=dpi)
    for idx in np.arange(n_samples):
        ax = fig.add_subplot(rows, cols, idx + 1, xticks=[], yticks=[])

        if labels is None:
            plt.imshow(
                masks[idx].permute(1, 2, 0), extent=extent, cmap="gray", vmin=0, vmax=1
            )
        else:
            mask = masks[idx][0] * (label_map[int(labels[idx].item())])
            plt.imshow(
                mask,
                cmap="turbo",
                extent=extent,
                vmin=0,
                vmax=max_label,
            )

        rating = ratings[idx]
        ax.set_title(
            rating if isinstance(rating, str) else f"{ratings[idx]:.4f}",
            fontdict={
                "fontsize": 6 if y < 128 else 18,
                "color": "orange" if labels is None else "white",
            },
            y=0.2 if isinstance(rating, str) else 0.35,
        )

    fig.patch.set_facecolor("#292929")
    height_px = y * rows
    width_px = x * cols
    fig.set_size_inches(width_px / (dpi / 2), height_px / (dpi / 2), forward=True)
    fig.tight_layout(pad=0)

    return fig


def visualize_sort_mean_std(
    data_loader: DataLoader,
    metric_fn: Callable[[Tensor, any], tuple[tuple[float, float], tuple[float, float]]],
    metric_name: str,
    *fn_args: any,
    plot_ratings=False,
    **fn_kwargs: any,
):
    n_samples = len(data_loader.sampler)
    masks = torch.zeros((n_samples, 3, 256, 512))
    ratings = torch.zeros((n_samples,))
    gb_means = torch.zeros((n_samples, 64))
    gb_stds = torch.zeros((n_samples, 64))

    for i, (mask, _) in enumerate(data_loader, 0):
        rating, mean_std = metric_fn(mask, *fn_args, **fn_kwargs)

        masks[i, :, :, :256] = F.resize(mask, (256, 256))[0]
        ratings[i] = rating

        gb_means[i] = mean_std[0][0]
        gb_stds[i] = mean_std[1][0]

        fig = plt.figure(figsize=(1, 1), dpi=256)
        # plt.bar(np.arange(64) - 0.25, gb_stds[i].cpu(), width=1.0)  # 64 bn..
        plt.bar(np.arange(64), gb_means[i].cpu())  # 64 bn..
        plt.ylim((-2, 2))
        plt.axis("off")
        plt.xticks(None)
        plt.tick_params(axis="y", labelsize=6)
        plt.tight_layout(pad=0)
        io_buf = io.BytesIO()
        fig.savefig(io_buf, format="raw", dpi=256)
        io_buf.seek(0)
        img_arr = (
            np.reshape(
                np.frombuffer(io_buf.getvalue(), dtype=np.uint8),
                newshape=(int(fig.bbox.bounds[3]), int(fig.bbox.bounds[2]), -1),
            ).astype(np.float32)
            / 255.0
        )
        masks[i, :, :, 256:] = torch.tensor(img_arr[:, :, :3]).permute((2, 0, 1))
        io_buf.close()
        plt.close()

    sort_idx = torch.argsort(ratings)

    std_means = gb_stds.mean(dim=1)
    print(std_means.shape)

    if plot_ratings:
        plt.plot(np.arange(len(ratings)), np.sort(ratings.numpy()))
        plt.xlabel("images")
        plt.ylabel(f"{metric_name} rating")
        plt.savefig(f"results/{metric_name}_rating_plot.png")
        plt.close()

    plt.plot(np.arange(len(ratings)), ratings[sort_idx])
    # plt.plot(np.arange(len(ratings)), mean_means[:, 0][sort_idx], label="gb mean")
    # plt.plot(np.arange(len(ratings)), mean_means[:, 1][sort_idx], label="lb mean")
    plt.plot(
        np.arange(len(ratings)),
        std_means[sort_idx],
        label="mean of 64 bottleneck std activation",
    )
    # plt.plot(
    #     np.arange(len(ratings)),
    #     std_means[:, 1][sort_idx],
    #     label="mean of 16 bottleneck std activation",
    # )
    plt.xlabel("images")
    plt.ylabel(f"{metric_name} rating")
    plt.legend()
    plt.savefig(f"results/{metric_name}_mean_std.png")
    plt.close()

    fig = plot_samples(masks[sort_idx], ratings[sort_idx])
    fig.savefig(f"results/{metric_name}_sort.png")
    plt.close(fig)

    gb_std_mean_sort_idx = np.argsort(std_means)
    # lb_std_mean_sort_idx = np.argsort(std_means[:, 1])
    fig = plot_samples(masks[gb_std_mean_sort_idx], std_means[gb_std_mean_sort_idx])
    fig.savefig(f"results/{metric_name}_gb_std_mean_sort.png")
    plt.close(fig)
    # fig = plot_samples(
    #     masks[lb_std_mean_sort_idx], std_means[:, 1][lb_std_mean_sort_idx]
    # )
    # fig.savefig(f"results/{metric_name}_lb_std_mean_sort.png")
    # plt.close(fig)


def visualize_reconstructions(
    data_loader: DataLoader, model: CONVVAE, extrema_fn: Callable, plot_histogram=False
):
    n_samples = len(data_loader.sampler)
    decode_delta = [-2, -1.5, -1, -0.5, 0, 0.5, 1, 1.5, 2]
    # decode_delta = [-4, -3, -2, -1, 0, 1, 2, 3, 4]
    grids = torch.zeros((n_samples, 1, 64, (len(decode_delta) + 2) * 64))
    extrema = torch.zeros((n_samples,))
    stds = torch.zeros((n_samples, model.bottleneck))

    model.eval()

    with torch.no_grad():
        for i, (mask, _) in enumerate(data_loader, 0):
            mask = mask.to(model.device)
            reconstructions, extremum, std = model.decode_range(
                mask, decode_delta, extrema_fn
            )
            grids[i, 0, :, :64] = mask[0]
            grids[i, 0, :, 64:] = reconstructions[0]
            extrema[i] = extremum
            stds[i] = std

    if plot_histogram:
        all_stds = stds.flatten()
        n, bins, _ = plt.hist(all_stds.numpy(), 40)
        plt.savefig(f"results/{extrema_fn.__name__}_hist.png")
        plt.clf()

    fig = plot_samples(grids, extrema)
    fig.savefig(f"results/altered_{extrema_fn.__name__}_reconstruction_vis.png")
    plt.close(fig)


def visualize_sort(
    data_loader: DataLoader,
    metric_fn: Callable[[Tensor, any], tuple[torch.float32, Tensor]],
    metric_name: str,
    *fn_args: any,
    plot_ratings=False,
    rows=10,
    cols=10,
    **fn_kwargs: any,
):
    n_samples = len(data_loader.sampler)
    recon_masks = None
    masks = torch.zeros((n_samples, 1, 64, 64))
    ratings = torch.zeros((n_samples,))

    plot_recons = True

    for i, (mask, _) in enumerate(data_loader, 0):
        rating, mask_recon_grid = metric_fn(mask, *fn_args, **fn_kwargs)
        if plot_recons and mask_recon_grid == None:
            plot_recons = False
        elif plot_recons and recon_masks is None:
            recon_masks = torch.zeros((n_samples, *mask_recon_grid.shape))

        masks[i] = mask[0]
        ratings[i] = rating

        if plot_recons:
            recon_masks[i] = mask_recon_grid

    if plot_ratings:
        plt.plot(np.arange(len(ratings)), np.sort(ratings.numpy()))
        plt.xlabel("images")
        plt.ylabel(f"{metric_name} rating")
        plt.savefig(f"results/{metric_name}_rating_plot.png")
        plt.close()

    sort_idx = torch.argsort(ratings)

    masks_sorted = masks[sort_idx]
    fig = plot_samples(masks_sorted, ratings[sort_idx], rows=rows, cols=cols)
    fig.savefig(f"results/{metric_name}_sort.png")
    plt.close(fig)

    if plot_recons:
        recon_masks_sorted = recon_masks[sort_idx]
        fig_recon = plot_samples(
            recon_masks_sorted, ratings[sort_idx], rows=rows, cols=cols
        )
        fig_recon.savefig(f"results/{metric_name}_sort_recon.png")
        plt.close(fig_recon)


def visualize_sort_multidim(
    data_loader: DataLoader,
    measures: list[
        tuple[str, Callable[[Tensor, any], tuple[torch.float32, Tensor]], any]
    ],
    rows=10,
    cols=10,
    max_norm=False,
    use_labels=False,
):
    n_samples = len(data_loader.sampler)
    n_dim = len(measures)
    masks = torch.zeros((n_samples, 1, 64, 64))
    ratings = torch.zeros((n_samples, n_dim))
    labels = torch.zeros((n_samples,))

    for i, (mask, label) in enumerate(data_loader, 0):
        masks[i] = mask[0]
        labels[i] = label

        for m in range(n_dim):
            _, fn, args = measures[m]
            rating, _ = fn(mask, *args)
            ratings[i, m] = rating

    if max_norm:
        mins = ratings.min(dim=0)
        maxs = ratings.max(dim=0)
        ratings[:] -= mins.values
        ratings[:] /= maxs.values - mins.values

    if n_dim == 3:
        fig = plt.figure()
        ax = fig.add_subplot(projection="3d")
        ax.scatter(ratings[:, 0], ratings[:, 1], ratings[:, 2], marker="o")

        ax.set_xlabel(measures[0][0])
        ax.set_ylabel(measures[1][0])
        ax.set_zlabel(measures[2][0])
        plt.savefig(f"results/3d_plot{'_norm' if max_norm else ''}.png")
        plt.close()

    measure_norm = torch.linalg.vector_norm(ratings, dim=1)
    sort_idx = np.argsort(np.array(measure_norm))
    rating_strings = [f"{r[0]:.4f}\n{r[1]:.4f}\n{r[2]:.4f}" for r in ratings[sort_idx]]

    fig = plot_samples(
        masks[sort_idx],
        rating_strings,
        labels=labels[sort_idx] if use_labels else None,
        rows=rows,
        cols=cols,
    )
    fig.savefig(
        f"results/{n_dim}dim_{'_'.join([m[0] for m in measures])}_sort{'_norm' if max_norm else ''}.png"
    )
    plt.close(fig)
