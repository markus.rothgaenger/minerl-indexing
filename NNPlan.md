## data structures
```
graph {
    vertices: Node(visit_count: int, o_t: rgb image as array(64, 64, 3))[]
    edges: Edge(i: Node index, j: Node index, d: output of decoder from observation i to j)[]
}
```

## libraries/model

there are models for mobilenet (v1 and v2) implemented in keras:

- https://keras.io/api/applications/mobilenet/

mobilenet v2 is included in pytorch:

- https://pytorch.org/hub/pytorch_vision_mobilenet_v2/
- add fully connected layer to implementation?! https://github.com/pytorch/vision/blob/main/torchvision/models/mobilenetv2.py
    - from 1024 to 128 for encoding
    - from 1088 to 6 for decoding
- use exponential activation -> https://pytorch.org/docs/stable/generated/torch.exp.html
- apply exponential function to 128 dim output of linear fully connected layer in forward pass?


## training

- minerl provides observations in shape (64, 64, 3) including actions. structure is dependant on environment and documented!

- maximization target:
    - second term -> kullback-leibler loss -> https://pytorch.org/docs/stable/generated/torch.nn.KLDivLoss.html
    - what does the first term represent? might be in https://arxiv.org/pdf/1612.00410.pdf